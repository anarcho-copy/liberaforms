# Install LiberaForms

If you want to use Docker, please follow the instructions in `docs/docker.md`

## Preparation

Requires
* Python3.7 or greater.
* A PostgreSQL server

Install required system packages
```
apt-get install git python3-venv python3-dev gcc libpq-dev mime-support
```

## Create a user that will run LiberaForms

You might want to run LiberaForms with the user `www-data`. If not you can create a user like this.

```
adduser --disabled-password liberaforms
```

# Install LiberaForms

You can install LiberaForms in the directory of your choice.


## Clone LiberaForms

```
cd /opt
git clone https://gitlab.com/liberaforms/liberaforms.git liberaforms
cd liberaforms
chown liberaforms logs uploads
```

Make sure the user who runs LiberaForms is the owner of the directories and files

## Create a Python venv

```
cd /opt/liberaforms
python3 -m venv ./venv
```

### Install python packages
```
source ./venv/bin/activate
pip install --upgrade pip
pip install -r ./requirements.txt
```

## Configure

### Create and edit `.env`
```
cp dotenv.example .env
```

You can create a SECRET_KEY like this
```
#openssl rand -base64 32
python -c 'import secrets; print(secrets.token_hex())'
```

Add the database config.
```
DB_HOST=localhost
DB_NAME=liberaforms
DB_USER=liberaforms
DB_PASSWORD=a_secret_db_password
```

### Database

Create a user and database with the `.env` values

```
flask database create
```

Another way to do the same is this
```
sudo su
su postgres -c "liberaforms/commands/postgres.sh create-db"
exit
```

#### Create tables

Upgrade the database to the latest version

```
flask db upgrade
```

See more db options here https://flask-migrate.readthedocs.io/en/latest/#api-reference


### Drop database

If you need to delete the database (warning: the database user is also deleted)

```
flask database drop
```

Or alternatively
```
sudo su
su postgres -c "liberaforms/commands/postgres.sh drop-db"
exit
```

## Data encryption

LiberaForms encrypts passwords by default.
However, these other values are also encrypted:

* Form attachments when they are submitted
* Fediverse authentification

You need to create a key for these features to be enabled.

### Create the key

```
flask cryptokey create

olYyUwGT--example-key--c9AkH_HoMEWg9Q=

```

> Important. Save this key somewhere safe and do not lose it!

Copy the generated key and save it in a file with a name you will recognize.
Something like `my.domain.com.key`.

Now add the key you have generated to your `.env` file

```
CRYPTO_KEY=olYyUwGT--example-key--c9AkH_HoMEWg9Q=
```


## SMTP config

You can configure SMTP via the CLI (see docs/CLI.md)

Optionally, SMTP can be configued via the web UI by the `ROOT_USER`

### Session data

```
SESSION_TYPE = "filesystem"
#SESSION_TYPE = "memcached"
```

If you use `filesystem` you must ensure the user running LiberaForms has write permissions
on the directory. For example

```
chown liberaforms ./flask_session
```

## Configure Gunicorn

Gunicorn serves LiberaForms.

This command suggests a configuration file path and it's content.

```
flask config hint gunicorn
```

Copy the content. Create the file `./gunicorn.py` and paste. Change the `user` name if needed


## Install Supervisor

Supervisor manages Gunicorn. It will start the process when the server boots.
```
sudo apt-get install supervisor
```

### Configure Supervisor

This command suggests a configuration file path and it's content.

```
flask config hint supervisor
```

Copy the content. Create the `liberaforms.conf` file and paste. Change the `user` name if needed

Restart supervisor and check if LiberaForms is running.

```
sudo systemctl restart supervisor
sudo supervisorctl status liberaforms
```

Other supervisor commands

```
sudo supervisorctl start liberaforms
sudo supervisorctl stop liberaforms
```

# Backups

## Database

Create a directory for the backup and set permissions

```
mkdir /var/backups/liberaforms
chown postgres /var/backups/liberaforms/
```

Run this and check if a copy is dumped correctly.
```
su postgres -c "/usr/bin/pg_dump <db_name> > /var/backups/liberaforms/backup.sql"
```

Add a line to postgres user's crontab
```
crontab -u postgres -e
```
to run it every night..
```
30 3 * * * /usr/bin/pg_dump -U <db_name> > /var/backups/liberaforms/backup.sql"
```

Note: This overwrites the last copy. You might want to change that.

Don't forget to check that the cronjob is dumping the database correctly.

### Recover database from a backup

```
sudo su
su postgres
cat <sql_dump_file> | psql -U postgres <db_name>
```

## Uploaded files

If you enabled `ENABLE_UPLOADS` in the `.env` file, LiberaForm will save
uploaded files in the `./uploads` directory, you should make copies of this directory.

## CRYPTO_KEY

Do not lose it!

# Web server

You need to configure a web server to serve LiberaForms.

Configure nginx proxy. See `docs/nginx.example`

(TODO: include an apache2 config)


# Installation finished!

Stop the flask server if you still have it running in a terminal.

Start LiberaForms

```
supervisorctl start liberaforms
```

# Bootstrap the first admin user
So now everything is working and you need to create the first user.

`ROOT_USER` defined in the `.env` can be used to create the first Admin user.

1. From the Login page, choose 'Forgot your password?' link
2. Enter the ROOT_USER email
3. Fill out the new user form
4. Go to the configuration page and get the SMTP config working first

# Debugging
Something gone wrong?

You can check supervisor's log at `/var/log/supervisor/...`

You can also run LiberaForms in debug mode.
```
sudo supervisorctl stop liberaforms
FLASK_ENV=development flask run
```

To test gunicorn
```
gunicorn --worker-tmp-dir /dev/shm --bind 0.0.0.0:5000 --workers 3 wsgi:app -e ENV_FILE=.env
```
