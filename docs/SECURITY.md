# Security

We do our best to make LiberaForms secure.

If you find a security problem with this software please email us at security@liberaforms.org

Thank you!
