"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""


from flask import Blueprint, redirect
from flask import current_app, flash, g, url_for
from flask_babel import gettext as _
import flask_login
from liberaforms.models.invite import Invite
from liberaforms.utils import auth

invite_bp = Blueprint('invite_bp', __name__)


@invite_bp.route('/invite/<int:invite_id>/delete', methods=['GET'])
@auth.enabled_user_required
def delete_invite(invite_id):
    invite=Invite.find(id=invite_id)
    if invite:
        if invite.granted_form and invite.invited_by_id == flask_login.current_user.id:
            form_id = invite.granted_form['id']
            invite.delete()
            # i18n: %s is an email address
            flash(_("Invitation to %s deleted OK" % invite.email), 'success')
            return redirect(url_for('form_bp.share_form', form_id=form_id))
        if g.is_admin:
            invite.delete()
            flash(_("Invitation to %s deleted OK" % invite.email), 'success')
            return redirect(url_for('admin_bp.list_invites'))
        current_app.logger.warning(f"Failed to delete Invite. id: {invite_id}")
    flash(_("Opps! We can't find that invitation"), 'error')
    if g.is_admin:
        return redirect(url_for('admin_bp.list_invites'))
    return redirect(url_for('form_bp.forms'))
