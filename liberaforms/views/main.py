"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import Blueprint
from flask import render_template
from flask import g

main_bp = Blueprint('main_bp',
                    __name__,
                    template_folder='../templates/main')


@main_bp.route('/', methods=['GET'])
def index():
    return render_template('index.html', site=g.site)
