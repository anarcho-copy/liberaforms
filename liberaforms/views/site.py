"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, json
import mimetypes
from feedgen.feed import FeedGenerator
from flask import g, request, render_template, redirect, Response
from flask import Blueprint, current_app, url_for
from flask import session, flash, jsonify
from flask_babel import gettext as _
import flask_login

from liberaforms.models.site import Site
from liberaforms.models.invite import Invite
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.domain.user import UserDomain
from liberaforms.utils import auth
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import tokens
from liberaforms.utils import html_parser
from liberaforms.utils.chart_data import get_site_statistics
from liberaforms.utils.dispatcher import Dispatcher
import liberaforms.utils.wtf as wtf

from pprint import pprint

site_bp = Blueprint('site_bp', __name__,
                    template_folder='../templates/site')


@site_bp.route('/site/save-blurb', methods=['POST'])
@auth.enabled_admin_required
def save_blurb():
    if 'editor' in request.form:
        g.site.save_blurb(request.form['editor'])
        flash(_("Text saved OK"), 'success')
    return redirect(url_for('main_bp.index'))


@site_bp.route('/site/recover-password', methods=['GET', 'POST'])
@site_bp.route('/site/recover-password/<string:token>', methods=['GET'])
@auth.anonymous_user_required
@tokens.sanitized_token
def recover_password(token=None):
    if token:
        user = User.find(token=token)
        if not user:
            flash(_("Couldn't find that token"), 'warning')
            return redirect(url_for('main_bp.index'))
        if tokens.has_token_expired(user.token):
            flash(_("Your petition has expired"), 'warning')
            user.delete_token()
            return redirect(url_for('main_bp.index'))
        if user.blocked:
            user.delete_token()
            flash(_("Your account has been blocked"), 'warning')
            return redirect(url_for('main_bp.index'))
        user.delete_token()
        user.validated_email=True
        user.save()
        flask_login.login_user(user)
        return redirect(url_for('user_bp.reset_password'))

    wtform=wtf.GetEmail()
    if wtform.validate_on_submit():
        user = User.find(email=wtform.email.data, blocked=False)
        if user:
            user.save_token()
            Dispatcher().send_account_recovery(user)
        if not user and wtform.email.data == current_app.config['ROOT_USER']:
            if not User.find(email=wtform.email.data):
                # auto invite root user
                invite=Invite(  email=wtform.email.data,
                                message="New root user",
                                token=tokens.create_token(Invite),
                                role='admin',
                                invited_by_id=None)
                invite.save()
                return redirect(url_for('user_bp.create_new_user',
                                        invite=invite.token['token']))
        flash(_("We may have sent you an email"), 'info')
        return redirect(url_for('main_bp.index'))
    return render_template('recover-password.html', wtform=wtform)


@site_bp.route('/site/terms-and-conditions', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_terms_and_conditions():
    if request.method == 'POST':
        if 'markdown' in request.form and "checkbox_text" in request.form:
            markdown=sanitizers.bleach_text(request.form['markdown'])
            if not markdown:
                markdown = _("Please accept our terms and conditions.")
            g.site.save_terms_and_conditions(
                markdown = markdown,
                html = sanitizers.markdown2HTML(markdown),
                checkbox_text = sanitizers.remove_html_tags(request.form['checkbox_text'])
            )
            flash(_("Terms and conditions saved OK"), 'success')
    return render_template('terms-and-conditions.html')


@site_bp.route('/site/toggle-terms-and-conditions', methods=['POST'])
@auth.enabled_admin_required
def toggle_terms_and_conditions():
    return jsonify({'enabled': g.site.toggle_terms_and_conditions()})


@site_bp.route('/site/data-consent', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_data_consent():
    if request.method == 'POST':
        if 'markdown' in request.form and "checkbox_text" in request.form:
            markdown=sanitizers.bleach_text(request.form['markdown'])
            kwargs = {
                "markdown": markdown,
                "checkbox_text": sanitizers.remove_html_tags(request.form['checkbox_text']),
                "required": "True"
            }
            g.site.save_consent(kwargs)
            flash(_("GDPR text saved OK"), 'success')
    return render_template('site/data-consent.html')


@site_bp.route('/site/toggle-data-consent', methods=['POST'])
@auth.enabled_admin_required
def toggle_data_consent():
    return jsonify({'enabled': g.site.toggle_consent_enabled()})


@site_bp.route('/site/toggle-share-data-consent', methods=['POST'])
@auth.enabled_admin_required
def toggle_share_data_consent():
    return jsonify({'shared': g.site.toggle_consent_shared()})


@site_bp.route('/site/preview-new-user-form', methods=['GET'])
@auth.enabled_admin_required
def preview_new_user_form():
    return render_template('new-user.html', wtform=wtf.NewUser(), preview_only=True)


@site_bp.route('/site/change-sitename', methods=['GET', 'POST'])
@auth.enabled_admin_required
def change_name():
    if request.method == 'POST' and 'sitename' in request.form:
        g.site.name=request.form['sitename']
        g.site.save()
        flash(_("Site name changed OK"), 'success')
        return redirect(url_for('admin_bp.site_admin'))
    return render_template('change-sitename.html', site=g.site)


@site_bp.route('/site/change-default-language', methods=['GET', 'POST'])
@auth.enabled_admin_required
def change_default_language():
    if request.method == 'POST':
        if 'language' in request.form \
            and request.form['language'] in current_app.config['LANGUAGES']:
            g.site.language=request.form['language']
            g.site.save()
            flash(_("Language updated OK"), 'success')
            return redirect(url_for('admin_bp.site_admin'))
    return render_template('common/change-language.html',
                            current_language=g.site.language,
                            go_back_to_admin_panel=True)

@site_bp.route('/site/change-icon', methods=['GET', 'POST'])
@auth.enabled_admin_required
def change_icon():
    if request.method == 'POST':
        if not request.files['file']:
            flash(_("Required file is missing"), 'warning')
            return render_template('change-icon.html')
        file=request.files['file']
        if "image/" in file.content_type:
            try:
                g.site.change_favicon(file)
                flash(_("Logo changed OK. Refresh with  &lt;F5&gt;"), 'success')
                return redirect(url_for('admin_bp.site_admin'))
            except Exception as error:
                current_app.logger.error(error)
        else:
            flash(_("An image file is required"), 'warning')
            return render_template('change-icon.html')
    return render_template('change-icon.html')


@site_bp.route('/site/reset-favicon', methods=['GET'])
@auth.enabled_admin_required
def reset_site_favicon():
    if g.site.reset_favicon():
        flash(_("Logo reset OK. Refresh with  &lt;F5&gt;"), 'success')
    return redirect(url_for('admin_bp.site_admin'))


@site_bp.route('/site/edit-mimetypes', methods=['GET', 'POST'])
@auth.enabled_admin_required
def edit_mimetypes():
    wtform=wtf.FileExtensions()
    if wtform.validate_on_submit():
        mimetypes.init()
        updated_mimetypes = {"extensions":[], "mimetypes": []}
        extensions = wtform.extensions.data.splitlines()
        for extension in extensions:
            if not extension:
                continue
            mime_type = mimetypes.types_map[f".{extension}"]
            if not extension in updated_mimetypes["extensions"]:
                updated_mimetypes["extensions"].append(extension)
                updated_mimetypes["mimetypes"].append(mime_type)
        g.site.mimetypes = updated_mimetypes
        g.site.save()
        flash(_("Enabled file extensions updated OK"), 'success')
        return redirect(url_for('admin_bp.site_admin'))
    if request.method == 'GET':
        wtform.extensions.data = '\n'.join(g.site.mimetypes['extensions'])
    return render_template('edit-mimetypes.html', wtform=wtform)


@site_bp.route('/site/toggle-invitation-only', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_invitation_only():
    return jsonify(invite=g.site.toggle_invitation_only())


@site_bp.route('/site/toggle-newuser-uploads-default', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newuser_uploads_default():
    return jsonify(uploads=g.site.toggle_newuser_uploads_default())


@site_bp.route('/site/primary-color', methods=['GET', 'POST'])
@auth.enabled_admin_required
def primary_color():
    wtform=wtf.ChangePrimaryColor()
    if request.method == 'GET':
        wtform.hex_color.data=g.site.primary_color
    if wtform.validate_on_submit():
        g.site.primary_color=wtform.hex_color.data
        g.site.save()
        flash(_("Color changed OK"), 'success')
        return redirect(url_for('admin_bp.site_admin'))
    return render_template('set-primary-color.html', wtform=wtform)


@site_bp.route('/site/edit-new-form-message', methods=['GET', 'POST'])
@auth.enabled_admin_required
def new_form_msg():
    wtform=wtf.NewFormMessage()
    languages = []
    for language_code, value in current_app.config['LANGUAGES'].items():
        languages.append((language_code, value[0]))
    wtform.language.choices=languages
    if wtform.validate_on_submit():
        g.site.new_form_msg[wtform.language.data]=wtform.msg.data
        g.site.save()
        flash(_("New form message changed OK"), 'success')
    if not wtform.language.data:
        wtform.language.data = g.language
    messages = {}
    previews = {}
    for lang_code in g.site.new_form_msg:
        messages[lang_code] = g.site.new_form_msg[lang_code]
        previews[lang_code] = g.site.get_new_form_msg(lang_code)
    return render_template('change-new-form-msg.html',
                            wtform=wtform,
                            messages=messages,
                            previews=previews)


@site_bp.route('/site/edit-contact-info', methods=['GET', 'POST'])
@auth.enabled_admin_required
def contact_info():
    wtform=wtf.ContactInformation()
    if request.method == 'GET':
        wtform.contact_info.data=g.site.contact_info
    if wtform.validate_on_submit():
        g.site.contact_info=wtform.contact_info.data
        g.site.save()
        flash(_("Contact information changed OK"), 'success')
        return redirect(url_for('admin_bp.site_admin', _anchor='contact_info'))
    return render_template('change-contact-info.html', wtform=wtform)


@site_bp.route('/site/stats', methods=['GET'])
@auth.enabled_admin_required
def stats():
    return render_template('stats.html',
                            site=g.site,
                            stats=get_site_statistics())

## SMTP config

@site_bp.route('/site/email/config', methods=['GET', 'POST'])
@auth.enabled_admin_required
def smtp_config():
    wtf_smtp=wtf.smtpConfig(**g.site.smtp_config)
    if wtf_smtp.validate_on_submit():
        if not wtf_smtp.encryption.data == "None":
            encryption = wtf_smtp.encryption.data
        else:
            encryption = ""
        config={}
        config['host'] = wtf_smtp.host.data
        config['port'] = wtf_smtp.port.data
        config['encryption'] = encryption
        config['user'] = wtf_smtp.user.data
        config['password'] = wtf_smtp.password.data
        config['noreplyAddress'] = wtf_smtp.noreplyAddress.data
        g.site.save_smtp_config(**config)
        flash(_("Confguration saved OK"), 'success')
    wtf_email=wtf.GetEmail()
    return render_template('smtp-config.html',
                            wtf_smtp=wtf_smtp,
                            wtf_email=wtf_email)


@site_bp.route('/site/email/test-config', methods=['POST'])
@auth.enabled_admin_required
def test_smtp():
    wtform=wtf.GetEmail()
    if wtform.validate_on_submit():
        status = Dispatcher().send_test_email(wtform.email.data)
        if status['email_sent'] == True:
            flash(_("SMTP config works!"), 'success')
        else:
            flash(status['msg'], 'warning')
    else:
        flash("Email not valid", 'warning')
    return redirect(url_for('site_bp.smtp_config'))

## LDAP config

@site_bp.route('/site/ldap', methods=['GET'])
@auth.enabled_admin_required
def ldap_test():
    if not current_app.config['ENABLE_LDAP']:
        flash(_("LDAP config is not enabled"), 'error')
        return redirect(url_for('admin_bp.site_admin'))
    return render_template('ldap-test.html')

@site_bp.route('/site/ldap/edit-filter', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_edit_filter():
    if 'filter' in request.form and request.form['filter']:
        if not '%uid' in request.form['filter']:
            return jsonify(error="%uid is not declared in the filter")
        g.site.ldap_filter = request.form['filter']
        g.site.save()
    return jsonify(filter=g.site.get_ldap_filter())

@site_bp.route('/site/ldap/reset-filter', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_reset_filter():
    g.site.ldap_filter = None
    g.site.save()
    return jsonify(filter=g.site.get_ldap_filter())

@site_bp.route('/site/ldap/test-bind', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_test_bind():
    username = None
    if 'username' in request.form:
        username = sanitizers.sanitize_username(request.form['username'])
    password = request.form['password'] if 'password' in request.form else None
    domain = UserDomain(username, password)
    conn, result = domain.ldap_bind()
    domain.ldap_unbind(conn)
    return jsonify(result=result)

@site_bp.route('/site/ldap/test-search', methods=['POST'])
@auth.enabled_admin_required__json
def ldap_test_search():
    username = None
    if 'username' in request.form:
        username = sanitizers.sanitize_username(request.form['username'])
    password = request.form['password'] if 'password' in request.form else None
    domain = UserDomain(username, password)
    conn, msg = domain.ldap_bind()
    result, msg = domain.ldap_search(conn, request.form['param'])
    domain.ldap_unbind(conn)
    result.append(msg)
    return jsonify({'result': result })

# RSS

@site_bp.route('/feed', methods=['GET'])
@site_bp.route('/feed/<string:feed_type>', methods=['GET'])
def rss_feed(feed_type=None):
    """RSS feed of latest 10 published forms. Public information only."""
    if not current_app.config['ENABLE_RSS_FEED']:
        return redirect(url_for('main_bp.index'))
    feed_type = feed_type.lower() if feed_type else "rss"
    feed_type = feed_type if feed_type in ['rss', 'atom'] else "rss"
    fg = FeedGenerator()
    # i18n: First %s is sitename. Second %s is Feed type: RSS or Atom
    title = _("%s %s feed" % (g.site.name, feed_type))
    fg.id(f"{current_app.config['BASE_URL']}/feed/{feed_type}")
    fg.title(title)
    fg.link(href=current_app.config['BASE_URL'])
    if feed_type == 'rss':
        fg.description(g.site.blurb['html'])
    if feed_type == 'atom':
        fg.description(html_parser.extract_text(g.site.blurb['html'],
                                                with_links=True))
    forms = Form.query.filter_by(enabled=True, restricted_access=False) \
                      .order_by(Form.created.desc()) \
                      .paginate(page=1, per_page=10) \
                      .items
    for form in forms:
        if not form.is_enabled():
            # disabled by admin
            continue
        feed_entry = fg.add_entry()
        feed_entry.id(form.url)
        feed_entry.pubDate(form.created)
        feed_entry.title(form.slug)
        feed_entry.link(href=form.url)
        if feed_type == 'rss':
            feed_entry.description(form.introduction_text['html'])
        if feed_type == 'atom':
            desc = html_parser.extract_text(form.introduction_text['html'],
                                            with_links=True)
            feed_entry.description(desc)
    if feed_type == 'rss':
        return Response(fg.rss_str(pretty=True), mimetype='application/rss+xml')
    else:
        return Response(fg.atom_str(pretty=True), mimetype='application/atom+xml')
