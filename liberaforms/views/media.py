"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import g, request, render_template, redirect
from flask import Blueprint, current_app, jsonify, url_for
from flask_babel import gettext as _
import flask_login
from liberaforms.models.media import Media
from liberaforms.models.schemas.media import MediaSchema
from liberaforms.utils import auth
from liberaforms.utils import utils
from liberaforms.utils import wtf

#from pprint import pprint

media_bp = Blueprint('media_bp',
                    __name__,
                    template_folder='../templates/media')


@media_bp.route('/media/save', methods=['POST'])
@auth.enabled_user_required__json
def save_media():
    if not current_app.config['ENABLE_UPLOADS']:
        return jsonify(image_url=""), 406
    wtform = wtf.UploadMedia()
    if not wtform.validate_on_submit():
        return jsonify(errors=wtform.errors), 406
    media = Media()
    saved = media.save_media(flask_login.current_user,
                             request.files['media_file'],
                             request.form['alt_text'])
    if saved:
        if flask_login.current_user.set_disk_usage_alert():
            flask_login.current_user.save()
        total_usage=flask_login.current_user.total_uploads_usage()
        percent="{:.2f} %".format((total_usage * 100) / flask_login.current_user.uploads_limit)
        return jsonify(
            media=MediaSchema().dump(media),
            usage_percent=percent,
            total_usage=total_usage,
            show_alert=bool(flask_login.current_user.alerts)
        ), 200
    return jsonify(False), 406

@media_bp.route('/user/<string:username>/media', methods=['GET'])
@auth.enabled_user_required
def list_media(username):
    if username != flask_login.current_user.username:
        return redirect(url_for('media_bp.list_media',
                                 username=flask_login.current_user.username))
    if not flask_login.current_user.uploads_enabled:
        return redirect(url_for('user_bp.user_settings',
                                 username=flask_login.current_user.username))
    return render_template('list-media.html',
                            human_readable_bytes=utils.human_readable_bytes,
                            wtform=wtf.UploadMedia())

@media_bp.route('/media/delete/<int:media_id>', methods=['POST'])
@auth.enabled_user_required__json
def remove_media(media_id):
    media = Media.find(id=media_id, user_id=flask_login.current_user.id)
    if media:
        removed = media.delete_media()
        if removed:
            if flask_login.current_user.set_disk_usage_alert():
                flask_login.current_user.save()
            total_usage=flask_login.current_user.total_uploads_usage()
            percent="{:.2f} %".format((total_usage * 100) / flask_login.current_user.uploads_limit)
            return jsonify(
                media_id=media.id,
                usage_percent=percent,
                total_usage=total_usage,
                show_alert=bool(flask_login.current_user.alerts)
            ), 200
    return jsonify(False), 404

@media_bp.route('/media/get-values/<int:media_id>', methods=['GET'])
@auth.enabled_user_required__json
def get_values(media_id):
    media = Media.find(id=media_id, user_id=flask_login.current_user.id)
    if media:
        return jsonify(media=MediaSchema().dump(media)), 200
    return jsonify(False), 404
