"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, json
from flask import current_app, g, request, render_template, redirect
from flask import session, flash, Blueprint, url_for, jsonify
from flask import send_file, after_this_request
from flask_babel import gettext as _
import flask_login
from liberaforms.domain.storage import StorageDomain
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.invite import Invite
from liberaforms.models.schemas.invite import InviteSchema
from liberaforms.utils import auth
from liberaforms.utils import utils
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils.chart_data import get_user_statistics
from liberaforms.utils.exports import write_users_csv
from liberaforms.utils import tokens
from liberaforms.utils import wtf

#from pprint import pprint

admin_bp = Blueprint('admin_bp', __name__,
                    template_folder='../templates/admin')


@admin_bp.route('/admin', methods=['GET'])
@auth.enabled_admin_required
def site_admin():
    return render_template('admin-panel.html',
                            user=flask_login.current_user,
                            StorageDomain=StorageDomain,
                            app_version=utils.get_app_version(),
                            site=g.site)

## User management

@admin_bp.route('/admin/users', methods=['GET'])
@auth.enabled_admin_required
def list_users():
    return render_template('list-users.html',
                            users=User.find_all(),
                            invites=Invite.find_all())

@admin_bp.route('/admin/user/<int:user_id>', methods=['GET'])
@auth.enabled_admin_required
def inspect_user(user_id):
    user=User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    return render_template('inspect-user.html',
                            user=user,
                            chart_data=get_user_statistics(user),
                            human_readable_bytes=utils.human_readable_bytes)


@admin_bp.route('/admin/user/<int:user_id>/set-role/<string:role>', methods=['POST'])
@auth.enabled_admin_required__json
def set_role(user_id, role):
    user=User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    if not role in ('guest', 'editor', 'admin'):
        return jsonify("Invalid role"), 406
    if user.username != flask_login.current_user.username:
        # current_user cannot remove their own admin permission
        if user.set_role(role):
            user.save()
    return jsonify(role=user.role), 200


@admin_bp.route('/admin/user/<int:user_id>/toggle-blocked', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_user_blocked(user_id):
    user=User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    if user.id == flask_login.current_user.id:
        # current_user cannot disable themself
        blocked=user.blocked
    else:
        blocked=user.toggle_blocked()
    return jsonify(blocked=blocked), 200


@admin_bp.route('/admin/user/<int:user_id>/toggle-uploads-enabled', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_uploads_enabled(user_id):
    user=User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    return jsonify(uploads_enabled=user.toggle_uploads_enabled()), 200


@admin_bp.route('/admin/user/<int:user_id>/set-upload-limit', methods=['GET', 'POST'])
@auth.enabled_admin_required
def set_user_upload_limit(user_id):
    user=User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    wtform = wtf.UserUploadLimit()
    if wtform.validate_on_submit():
        result = f"{wtform.size.data.strip()} {wtform.unit.data.strip()}"
        bytes_limit:int = utils.string_to_bytes(result)
        user.set_uploads_limit(bytes_limit)
        user.set_disk_usage_alert()
        user.save()
        flash(_("Uploads limit updated OK"), 'success')
        return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
    if request.method == 'GET':
        bytes_str = utils.human_readable_bytes(user.uploads_limit)
        wtform.size.data = float(bytes_str[:-3])
        wtform.unit.data = bytes_str[-2:]
    default = [current_app.config['DEFAULT_USER_UPLOADS_LIMIT'][:-3],
               current_app.config['DEFAULT_USER_UPLOADS_LIMIT'][-2:]]
    return render_template('user-upload-limit.html',
                            user=user, default=default, wtform=wtform)

@admin_bp.route('/admin/user/<int:user_id>/delete', methods=['GET', 'POST'])
@auth.enabled_admin_required
def delete_user(user_id):
    user=User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    if request.method == 'POST' and 'username' in request.form:
        if user.is_root_user():
            flash(_("Cannot delete root user"), 'warning')
            return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
        if user.id == flask_login.current_user.id:
            flash(_("Cannot delete yourself"), 'warning')
            return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
        if user.username == request.form['username']:
            user.delete_user()
            flash(_("Deleted user '%s'" % (user.username)), 'success')
            return redirect(url_for('admin_bp.list_users'))
        flash(_("Username does not match"), 'warning')
    return render_template('delete-user.html', user=user)


@admin_bp.route('/admin/users/csv', methods=['GET'])
@auth.enabled_admin_required
def csv_users():
    csv_file = write_users_csv(User.find_all())
    @after_this_request
    def remove_file(response):
        os.remove(csv_file)
        return response
    return send_file(csv_file, mimetype="text/csv", as_attachment=True)


## Form management

@admin_bp.route('/admin/forms', methods=['GET'])
@auth.enabled_admin_required
def list_forms():
    return render_template('list-forms.html', forms=Form.find_all())


@admin_bp.route('/admin/form/<int:form_id>/toggle-public', methods=['GET'])
@auth.enabled_admin_required
def toggle_form_public_admin_prefs(form_id):
    queried_form = Form.find(id=form_id)
    if not queried_form:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('form_bp.my_forms'))
    queried_form.toggle_admin_form_public()
    return redirect(url_for('form_bp.inspect_form', form_id=queried_form.id))


## Invitations

@admin_bp.route('/admin/invites', methods=['GET'])
@auth.enabled_admin_required
def list_invites():
    invites=InviteSchema(many=True).dump(Invite.find_all())
    return render_template('list-invites.html', invites=invites)


@admin_bp.route('/admin/invites/new', methods=['GET', 'POST'])
@auth.enabled_admin_required
def new_invite():
    wtform=wtf.NewInvite()
    if wtform.validate_on_submit():
        invite=Invite(email=wtform.email.data,
                      message=wtform.message.data,
                      token=tokens.create_token(Invite),
                      role=wtform.role.data,
                      invited_by_id=flask_login.current_user.id)
        invite.save()
        status = Dispatcher().send_invitation(invite)
        if status['email_sent'] is True:
            # i18n: %s is email address
            flash_text = _("We have sent an invitation to %s" % invite.email)
            flash(flash_text, 'success')
            return redirect(url_for('admin_bp.list_invites'))
        invite.delete()
        flash(status['msg'], 'warning')
    if not wtform.message.data:
        wtform.message.data=Invite.default_message()
    return render_template('invite/new-invite.html',
                            wtform=wtform,
                            default_role='editor',
                            total_invites=Invite.find_all().count())


## Personal Admin preferences

@admin_bp.route('/admin/toggle-newuser-notification', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newUser_notification():
    return jsonify(notify=flask_login.current_user.toggle_new_user_notification()), 200


@admin_bp.route('/admin/toggle-newform-notification', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newForm_notification():
    return jsonify(notify=flask_login.current_user.toggle_new_form_notification()), 200


## ROOT_USER functions

@admin_bp.route('/admin/form/<int:form_id>/change-author', methods=['GET', 'POST'])
@auth.rootuser_required
def change_author(form_id):
    queriedForm = Form.find(id=form_id)
    if not queriedForm:
        flash(_("Can't find that form"), 'warning')
        return redirect(url_for('user_bp.my_forms'))
    editors=FormUser.find_all(form_id=queriedForm.id, is_editor=True)
    if request.method == 'POST':
        author = queriedForm.author
        if not ('old_author_username' in request.form and \
                request.form['old_author_username']==author.username):
            flash(_("Current author incorrect"), 'warning')
            return render_template('change-author.html', form=queriedForm,
                                                         editors=editors)
        if 'new_author_username' in request.form:
            new_author=User.find(username=request.form['new_author_username'])
            if new_author:
                if new_author.enabled:
                    old_author=author
                    if queriedForm.change_author(new_author):
                        form_user = FormUser.find(form_id=form_id, user_id=new_author.id)
                        if form_user:
                            form_user.is_editor=True
                        else:
                            form_user=FormUser(form_id=form_id,
                                               user_id=new_author.id,
                                               notifications=new_author.new_form_notifications(),
                                               is_editor=True)
                        form_user.save()
                        # i18n: both %s are usernames
                        log_text = _("Changed author from %s to %s" % (
                                                        old_author.username,
                                                        new_author.username))
                        queriedForm.add_log(log_text)
                        flash(_("Changed author OK"), 'success')
                        return redirect(url_for('form_bp.inspect_form',
                                                     form_id=queriedForm.id))
                else:
                    # i18n: %s is username
                    flash(_("Cannot use %s. The user is not enabled" % (
                                    request.form['new_author_username']),
                         ), 'warning')
            else:
                flash(_("Can't find username %s" % (
                                request.form['new_author_username'])
                     ), 'warning')
    return render_template('change-author.html', form=queriedForm,
                                                 editors=editors)
