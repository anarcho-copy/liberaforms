"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from functools import wraps
from flask import current_app, request, jsonify
from flask import redirect, url_for, flash
from flask_babel import gettext as _
import flask_login
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser


def anonymous_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_anonymous:
            return f(*args, **kwargs)
        return redirect(url_for('main_bp.index'))
    return wrap

def authenticated_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated:
            return f(*args, **kwargs)
        return redirect(url_for('main_bp.index'))
    return wrap

def enabled_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.enabled:
            return f(*args, **kwargs)
        if flask_login.current_user.is_authenticated:
            current_app.logger.info(f'Disabled user denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return redirect(url_for('main_bp.index'))
    return wrap

def enabled_user_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.enabled:
            return f(*args, **kwargs)
        if flask_login.current_user.is_authenticated:
            current_app.logger.info(f'Disabled user denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return jsonify("Denied"), 401
    return wrap

def enabled_editor_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.enabled and \
           flask_login.current_user.is_editor():
            return f(*args, **kwargs)
        if flask_login.current_user.is_authenticated:
            current_app.logger.info(f'User denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return redirect(url_for('main_bp.index'))
    return wrap


def instantiate_form(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        form_user = FormUser.find(form_id=kwargs['form_id'],
                                  user_id=flask_login.current_user.id,
                                  is_editor=True)
        if not form_user:
            flash(_("Cannot find that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        kwargs['form'] = form_user.form
        return f(*args, **kwargs)
    return wrap

def instantiate_form__json(allow_guest=False):
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            query = {"form_id": kwargs['form_id'],
                     "user_id": flask_login.current_user.id}
            if allow_guest is False:
                query['is_editor'] = True
            form_user = FormUser.find(**query)
            if not form_user:
                return jsonify(None), 401
            kwargs['form'] = form_user.form
            return f(*args, **kwargs)
        return wrap
    return decorator

#def instantiate_form__json(f):
#    @wraps(f)
#    def wrap(*args, **kwargs):
#        form_user = FormUser.find(form_id=kwargs['form_id'],
#                                  user_id=flask_login.current_user.id,
#                                  is_editor=True)
#        if not form_user:
#            return jsonify(None), 401
#        kwargs['form'] = form_user.form
#        return f(*args, **kwargs)
#    return wrap

def enabled_editor_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.enabled and \
           flask_login.current_user.is_editor():
            return f(*args, **kwargs)
        if flask_login.current_user.is_authenticated:
            current_app.logger.info(f'Disabled user denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return jsonify("Denied"), 401
    return wrap

def enabled_admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_admin():
            return f(*args, **kwargs)
        if flask_login.current_user:
            current_app.logger.info(f'Non admin user denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return redirect(url_for('main_bp.index'))
    return wrap

def enabled_admin_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_admin():
            return f(*args, **kwargs)
        if flask_login.current_user:
            current_app.logger.info(f'Non admin user denied: {request.path}')
        else:
            current_app.logger.info(f'Anon user denied: {request.path}')
        return jsonify("Denied"), 401
    return wrap

def rootuser_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_root_user():
            return f(*args, **kwargs)
        return redirect(url_for('main_bp.index'))
    return wrap
