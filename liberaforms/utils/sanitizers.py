"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import re, markdown, html
from unidecode import unidecode
from functools import wraps
from flask import current_app, flash, render_template
from flask_babel import gettext as _
import flask_login
from bs4 import BeautifulSoup
import bleach
from liberaforms.utils import validators


def sanitize_string(string):
    string = unidecode(string)
    string = string.replace(" ", "")
    return re.sub('[^A-Za-z0-9\-\.]', '', string) #DeprecationWarning: invalid escape sequence \-

def sanitize_slug(slug) -> str:
    """Return a valid URL path string."""
    slug = slug.lower()
    slug = slug.replace(" ", "-")
    return sanitize_string(slug)

def sanitize_username(username):
    return sanitize_string(username)

def remove_html_tags(text:str) -> str:
    """Remove tags"""
    text=html.unescape(text)
    TAG_RE = re.compile(r'<[^>]+>')
    return TAG_RE.sub('', text).strip()

def markdown2HTML(markdown_text:str) -> str:
    markdown_text=remove_html_tags(markdown_text.strip())
    return markdown.markdown(markdown_text, extensions=['markdown.extensions.nl2br'])

def sanitize_label(text:str) -> str:
    text = remove_html_tags(text)
    text = remove_newlines(text)
    return text.strip()

def bleach_text(text:str) -> str:
    def remove_script_tags(text):
        soup=BeautifulSoup(text, features="html.parser")
        for script in soup.find_all("script"):
            script.decompose()
        return str(soup)
    text = html.unescape(text)
    text = remove_script_tags(text)
    tags=['a', 'br', 'div']
    attributes={'a': ['href'], 'div': [], 'br': []}
    text = bleach.clean(text, tags=tags, attributes=attributes).strip()
    text = bleach.linkify(text, skip_tags=None, parse_email=False)
    return text if remove_html_tags(text).strip() else ""

def remove_newlines(string:str) -> str:
    string = string.replace("\n", "")
    return string.replace("\r", "")

def remove_first_and_last_newlines(string:str) -> str:
    RE="^[\r\n]+|[\r\n]+$"
    return re.sub(RE, '', string)

def truncate_text(text:str, truncate_at:int=155) -> str:
    """Return first n chars of text.

    155 is the recommened opengraph length
    """
    text = text.strip('\n').replace('  ', ' ').strip(' ')
    if len(text) > truncate_at:
        text = f"{text[0:truncate_at-3]}..."
    return text

## route decorators

def sanitized_slug_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'slug' not in kwargs:
            if flask_login.current_user.is_authenticated:
                flash("No slug found!", 'error')
            return render_template('page-not-found.html'), 404
        if kwargs['slug'] in current_app.config['RESERVED_SLUGS']:
            if flask_login.current_user.is_authenticated:
                flash("Reserved slug!", 'warning')
            return render_template('page-not-found.html'), 404
        if kwargs['slug'] != sanitize_slug(kwargs['slug']):
            if flask_login.current_user.is_authenticated:
                flash("That's a nasty slug!", 'warning')
            return render_template('page-not-found.html'), 404
        return f(*args, **kwargs)
    return wrap

def sanitized_key_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not ('key' in kwargs and \
                kwargs['key'] == sanitize_string(kwargs['key'])):
            if flask_login.current_user.is_authenticated:
                flash(_("That's a nasty key!"), 'warning')
            return render_template('page-not-found.html'), 404
        else:
            return f(*args, **kwargs)
    return wrap
