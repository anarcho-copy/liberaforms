"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from flask import session, current_app
from flask_babel import gettext as _
import flask_login
from liberaforms.utils import sanitizers
from liberaforms.models.form import Form


def clear_session_form_data(data_id=None):
    data_id = data_id if data_id else 0
    session[data_id] = {
        'duplication_in_progress': False,
        'slug': "",
        'form_id': None,
        'formFieldIndex': [],
        'formStructure': [],
        'introductionTextMD': '',
        'afterSubmitText': {},
        'expiredText': {},
        'editModeAlert': False
    }
    return session[data_id]

def populate_session_with_form(form):
    data_id=form.id
    clear_session_form_data(data_id=data_id)
    session[data_id] = {
        'slug': form.slug,
        'form_id': str(form.id),
        'formFieldIndex': form.fieldIndex,
        'formStructure': form.structure,
        'introductionTextMD': form.introduction_text['markdown'],
        'afterSubmitText': form.after_submit_text,
        'expiredText': form.expired_text,
        'editModeAlert': flask_login.current_user.preferences['show_edit_alert']
    }
    return session[data_id]

def populate_session_form_duplicate(form):
    session_data=populate_session_with_form(form)
    data_id=0
    session[data_id] = session_data
    session[data_id]['slug']=""
    session[data_id]['form_id']=None
    session[data_id]['duplication_in_progress'] = True

def get_session_form_data(data_id=None):
    data_id = data_id if data_id else 0
    return session[data_id] if data_id in session else clear_session_form_data(data_id=None)

"""
Sanitize form elements
formbuilder has some bugs. Repair if needed.
"""
def repair_form_structure(structure, form=None):
    def get_unique_option_value(values, label):
        value = label.replace(" ", "-")
        value = sanitizers.sanitize_string(value)
        value = sanitizers.sanitize_label(value)
        if len(value) > 43:
            value = f"{value[:40]}..." # make value length 43 chars
        unique_values=[option["value"] for option in values if option["value"]]
        cnt = 1
        while value in unique_values:
            value = f"{value}__{cnt}"
            cnt = cnt + 1
        return value
    for element in structure:
        if "type" in element:
            if element['type'] == 'paragraph':
                if 'label' in element:
                    element["label"]=sanitizers.bleach_text(element["label"])
                if not ('label' in element and element["label"]):
                    element["label"]=_('Paragraph')
                continue
            if 'label' in element:
                element['label'] = sanitizers.sanitize_label(element["label"])
            if not ('label' in element and element["label"]):
                # i18n: Refers to the name of a form field
                element["label"]=_("Add an appropriate field name here")
            if 'description' in element:
                element['description'] = sanitizers.sanitize_label(element["description"])
            # formBuilder does not save select dropdown correctly
            if element["type"] == "select" and "multiple" in element:
                if element["multiple"] == False:
                    del element["multiple"]
            # formBuilder does not enforce values for checkbox groups, radio groups and selects.
            # we add a value (derived form the Label) when missing
            if  element["type"] == "checkbox-group" or \
                element["type"] == "radio-group" or \
                element["type"] == "select":
                options = []
                for option in element["values"]:
                    option["value"] = option["value"].strip()
                    if not option["label"] and not option["value"]:
                        continue
                    if option["label"]:
                        option["label"] = sanitizers.sanitize_label(option["label"])
                    if not option["label"] and option["value"]:
                        option["label"] = sanitizers.sanitize_label(option["value"])
                        options.append(option)
                        continue
                    if not form or form.is_public() == False:
                        if form:
                            saved_values=form.get_multichoice_options_with_saved_data()
                            if element["name"] in saved_values.keys():
                                if option["value"] in saved_values[element['name']]:
                                    options.append(option)
                                    continue
                        option["value"]=get_unique_option_value(element["values"],
                                                                option["label"])
                    options.append(option)
                element["values"] = options
    return structure

def is_slug_available(slug):
    if Form.find(slug=slug):
        return False
    if slug in current_app.config['RESERVED_SLUGS']:
        return False
    return True
