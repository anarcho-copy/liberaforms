"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import datetime, time, uuid
from functools import wraps
from flask import url_for, flash, redirect, render_template
from flask_babel import gettext as _
import flask_login
from liberaforms.utils import utils
from liberaforms.utils import validators


def create_token(persistent_class, **kwargs):
    """Create a unique token.

    persistent_class may be a User class or an Invite class
    """
    token_string = utils.gen_random_string()
    while persistent_class.find(token=token_string):
        token_string = utils.gen_random_string()
    created = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    result={'token': token_string, 'created': created}
    return {**result, **kwargs}

def has_token_expired(token_data:dict) -> bool:
    """Check token expiration."""
    token_created = datetime.datetime.strptime(token_data['created'],
                                               "%Y-%m-%d %H:%M:%S")
    token_age = datetime.datetime.now() - token_created
    if token_age.total_seconds() <= int(os.environ['TOKEN_EXPIRATION']):
        return False
    return True

def sanitized_token(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'token' in kwargs and not validators.is_valid_UUID(kwargs['token']):
            if flask_login.current_user.is_authenticated:
                flash(_("That's a nasty token!"), 'warning')
            return render_template('page-not-found.html'), 404
        else:
            return f(*args, **kwargs)
    return wrap

def instantiate_invite(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        from liberaforms.models.invite import Invite
        if 'invite' in kwargs:
            if not validators.is_valid_UUID(kwargs['invite']):
                flash(_("That's a nasty token!"), 'warning')
                return render_template('page-not-found.html'), 404
            invite=Invite.find(token=kwargs['invite'])
            if not invite:
                flash(_("Invitation not found"), 'warning')
                return redirect(url_for('main_bp.index'))
            if has_token_expired(invite.token):
                flash(_("This invitation has expired"), 'warning')
                invite.delete()
                return redirect(url_for('main_bp.index'))
            kwargs['invite'] = invite
        return f(*args, **kwargs)
    return wrap
