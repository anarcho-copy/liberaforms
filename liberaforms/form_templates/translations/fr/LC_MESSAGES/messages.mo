��    ;      �              �  	   �  	   �     �     �  -        ?  "   K     n     �     �     �     �     �     �     �     �       
        !  1   '     Y  (   g     �     �  B   �  $        *     9  
   E  O   P     �     �     �     �     �     �     �     �  !      
   "     -     <     M  
   [     f     n  >   }  
   �  	   �     �     �  	   �     �  
     1        H     ^     t  �  �  	   i
  	   s
     }
  2   �
  8   �
       5        L     i     �     �     �     �     �     �             
   "     -  F   6     }  ,   �  &   �  &   �  Y     1   j     �     �     �  w   �     P     X     l     t     x     �     �     �  '   �  
   �     �               #     0     9  P   E     �  	   �     �     �     �     �     �  <        ?     W     o   10h - 12h 12h - 14h A friend told me Add a second file if you wish. Add additional information about the project. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Comments Contact Form Cost justification Country Date for departure Date of arrival Double bed Email Explain here your project. Focus on what and how. Hotel booking How did you find out about this lottery? How should we call you? I receive your newletters I want you to erase all information if the project is not selected If you win, we'll send you an email! Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Message Name One day congress Privacy Project description Project name Put your name down to win a prize Single bed Summer courses Telephone number Thursday 27th Vegaterian Website Wednesday 26th What should we do if your project is not immediately selected? Your data. Your name e.g. +34 678 655 333 e.g. FediBook e.g. Mary e.g. Poppins e.g. Spain e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2022-04-07 12:06+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language: fr
Language-Team: French <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/fr/>
Plural-Forms: nplurals=2; plural=n > 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 10h - 12h 12h - 14h Un·e ami·e m’en a parlé Ajoutez un deuxième fichier si vous le souhaitez. Ajoutez des informations supplémentaires sur le projet. Pièces jointes Réservez une chambre et venez séjourner avec nous ! Informations de réservation Petit-déjeuner et dîner Petit-déjeuner et déjeuner En naviguant sur les internets Commentaires Formulaire de contact Justification des coûts Pays Date de départ Date d’arrivée Lit double Courriel Expliquez ici votre projet. Concentrez-vous sur le quoi et le comment. Réservation d’hôtel Comment avez-vous découvert cette loterie ? Comment devrions-nous vous appeler ? Je reçois vos lettres d’information Je veux que vous effaciez toutes les informations si le projet n’est pas sélectionné. Si vous gagnez, nous vous enverrons un courriel ! Juste le petit-déjeuner Juste le dîner Juste le déjeuner Laissez les participants choisir l'une des deux conférences qui se déroulent en parallèle et leur menu de déjeuner. Loterie Déjeuner et dîner Message Nom Congrès d'un jour Confidentialité Description du projet Nom du projet Inscrivez votre nom pour gagner un prix Lit simple Cours d’été Numéro de téléphone Jeudi 27 Végétarien Site Web Mercredi 26 Que devons-nous faire si votre projet n’est pas immédiatement sélectionné ? Vos données. Votre nom p. ex. : +34 678 655 333 p. ex. FediBook p. ex. Marie p. ex. Poppins p. ex. Espagne Par exemple, nous voulons écrire un livre sur le Fediverse. p. ex. https://fedi.cat p. ex. mary@example.com p. ex. mary@exemple.com 