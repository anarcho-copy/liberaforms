��    �      D              l
  �   m
  s   �
  g   o  (  �  O      �   P  :  @  �   {  .   �     ,     A     Z     s  +   |     �  	   �  5   �  N   �  4   9  	   n     x  +   �  &   �  &   �               
                                   /     ;     M  -   l  /   �  .   �  8   �     2  "   >     a     u     �     �     �     �     �               &     3     ;     N     V     i  D   y  &   �  
   �     �       1     3   @  (   t     �     �     �  (   �     �       M   '     u  B   �     �  !   �  	   
  $     
   9     D     S  
   _  O   j     �     �  "   �     �     �               ,     I     Z     g  !   y     �     �     �     �     �     �  !   �  (         I     e     v     �     �  "   �  -   �       +   "  
   N  L   Y     �     �     �     �  6   �               #  	   ,     6     C     P  
   V     a     i  '   x  >   �     �     �          /     K     g     �  "   �  #   �     �  
   �  
     	          )   0  %   Z     �  X   �  ,   �  U     =   j  :   �  $   �       	         *  
   7  /   B  1   r     �     �     �     �  �  �  �   �!  �   b"  �   �"  4  m#  X   �$  �   �$  *  �%  i   &'  /   �'  )   �'  "   �'     (     $(  5   2(     h(     j(  I   x(  J   �(  6   )     D)     R)  0   h)  ,   �)  ,   �)     �)     �)     �)     *     *     *     *     *     *     **     1*  -   H*  1   v*  *   �*  4   �*  6   +  
   ?+  *   J+      u+     �+     �+     �+      �+     �+     ,     -,  	   G,     Q,     l,     s,  
   �,     �,     �,  8   �,     �,     -  !   -     9-  K   @-  0   �-  -   �-     �-     .     .  $   ).     N.     d.  4   |.      �.  /   �.     /  #   /     >/  3   J/  	   ~/     �/     �/     �/  d   �/     0     "0  !   60     X0     ^0     d0     v0     �0     �0     �0     �0     �0     	1     1     '1     =1     W1     p1  "   �1     �1     �1     �1     �1  1   2     >2  "   W2  ;   z2     �2  -   �2     �2  J   	3  	   T3     ^3     o3     w3  .   �3     �3     �3     �3     �3     �3     �3     �3     4     4     4  '   %4  K   M4     �4      �4     �4     �4     5     *5  '   I5  %   q5  (   �5      �5     �5     �5  
   �5     6  &   6  -   A6     o6  j   �6  <   �6  G   (7  :   p7  5   �7     �7     8  
   8     '8     88  /   F8  7   v8     �8     �8     �8     �8   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Catalunya, Espanya

Phone number: (+34) 93 553 43 25 
[Website](https://elriuethotel.com)
[Map](https://www.openstreetmap.org/way/218557184)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Catalunya, Espanya

Phone number: (+34) 93 423 44 44 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No thanks. I will remember. No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 678 655 333 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com e.g. mary@exemple.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2022-04-08 10:01+0200
PO-Revision-Date: 2021-12-18 15:32+0000
Last-Translator: LiberaForms <info@liberaforms.org>
Language: eu
Language-Team: Basque <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/eu/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 # Jarduerarekiko iritzia
Eskerrik asko jarduera honetan parte hartzeagatik. Partekatu zure iritzia etorkizuneko jarduerak hobetu ahal izateko. # II. kongresua
Iazko kongresuak hainbesteko arrakasta izan zuen, aurten beste bat antolatu dugula.

Eman izena aurtengo saiorako! # Harremanetarako galdetegia
Galderarik duzu? Bete harremanetarako galdetegia eta zurekin harremanetan jarriko gara ahal bezain pronto. # Hotelerako erreserba
Etorri gurekin! Bete galdetegi hau gure hoteleko logela bat erreserbatzeko.

El Riuet Hotel
Carrer d'Orient, 33
Vidreres, 17411, Girona.
Katalunia, Espainia

Telefono zenbakia: (+34) 93 553 43 25
[Webgunea](https://elriuethotel.com)
[Mapa](https://www.openstreetmap.org/way/218557184)
 # Zozketa
Lortu zinemarako 5 sarrera! Erantzun galdetegi honi irailaren 20aren aurretik. # Proiektua proposatzeko galdetegia
Proiektu bat duzu? Bete galdetegi hau zure proposamenaren berri emateko.

Galdetegi hau proiektuak proposatzeko soilik da. Beste galderarik baduzu, erabili [harremanetarako galdetegia](https://example.com/contact-form). # Jatetxerako erreserba
Gure janari goxoa dastatu nahi? Bete galdetegi hau eta gorde data zure agendan!

[Jatetxe Goxorako mapa](https://www.openstreetmap.org/way/314016327)
Carrer de les Avellanes, 15
El Secà de Sant Pere, 25005, Lleida.
Katalunia, Espainia

Telefono zenbakia: (+34) 93 423 44 44 # Udako ikastaroak

Hiru egun beteko ditugu ikastaro eta tailerrez.

Erreserbatu zure tokia. Presa hartu! # Zure laguntza behar dugu
Sinatu gure eskaera. (sarrera-erregistroa 12:00etatik aurrera) (irteera-erregistroa 12:00ak arte) (asteartetik igandera) (egonez gero) (gogoratu soilik 20:30etik 23:30era irekitzen dugula) 1 10:00 - 12:00 10:00 - 13:00. Ordenagailu-laborategiaren kudeaketa, software librearekin 10:00 - 13:00. Sare neutroak. Gure WiFi instalazioaren aurkezpen praktikoa 10:00 - 13:00. Aurkezpena. TPV kafetegi bat kudeatzeko 12:00 - 14:00 14:00 -15:00 Bazkaria 16:00 - 19:00. Moneta sozialak. Bertako ekonomia 18:00 - 20:00. GIT hasiberrientzat, 1. saioa 18:00 - 20:00. GIT hasiberrientzat, 2. saioa 2 20:30 21:00 21:30 22:00 3 4 5 Lagun batek esan zidan Egokia Jarduerarekiko iritzia Nahi izanez gero, gehitu beste fitxategi bat. Gehitu proiektuari buruzko informazio gehigarria. Eskatu beka bat. Kontatu zuen proposamena. Eskatu herritarrei zure tokiko ekimenerako laguntza. Galdetu partaideei zure jarduerarekiko beren iritziaz. Eranskinak Erreserbatu logela bat eta etorri gurekin! Erreserbaren gaineko informazioa Gosaria eta afaria Gosaria eta bazkaria Interneten topatu nuen Hautatu etorri nahi duzuen eguna Irakasleari buruzko iruzkina Denborei buruzko iruzkina Edukiari buruzko iruzkina Iruzkinak Harremanetarako galdetegia Edukia Kostuen justifikazioa Herrialdea Irteera data Iritsiera data Gure janari goxoa dastatu nahi? Gorde data zure agendan! Nahi duzu abisurik bidaltzea? Birentzako ohea Birentzako ohea + Batentzako ohea Eposta Azaldu zure proiektua. Eman arreta egiten duzuen horri eta egiteko moduari. Azaldu zertarako erabiliko den eskatutako dirua. Otordu guztiak (gosaria, bazkaria eta afaria) Etorkizuneko jarduerak Ezer gehitu nahi? Hotelerako erreserba Nola eduki duzu zozketa honen berri? Zenbat izango zarete? Nola nahi duzu deitzea? Datuak gorde ditzakezue hurrengo diru-laguntzetarako Zuen berripaperak jasotzen ditut Proiektua ez bada onartua, datu guztiak ezabatu Beste nonbait jango dut Kanpoan egingo ditut, eskerrik asko ID zenbakia Irabazten baduzu, eposta mezua bat bidaliko dizugu! Irakaslea Gosaria soilik Afaria soilik Bazkaria soilik Utzi bertaratutakoei aldi berean emango diren bi hitzaldietako bat eta bazkarirako menua aukeratzen. Zozketa Bazkaria eta afaria Gehienez 10 pertsona erreserbako. Mezua Izena Izena edo ezizena Ez, mila esker. Oroituko naiz. Ez, mila esker. Oroituko naiz. Egun bakarreko kongresua Antolakundea Beste informazioa Besterik esan nahi baduzu… Txarretik onera Pribatutasuna Proiektua proposatzea Proiektuaren deskribapena Proiektuaren informazioa Proiektuaren izena Idatzi zure izena saria irabazteko Baloratu irakaslearen azalpenak Baloratu edukiaren kalitatea Eskatutako kopurua Jatetxerako erreserba 1 Gela. Adimen Artifizialak eragindako bazterketa 1. gela. Udal-estrategia 2 Gela. Eraiki webgune anonimo bat 2. gela. Teknologia deszentralizatua. Adierazpen-askatasuna Salbatu gure aterpea Logela partekatua (4 pertsonentzako ohatzeak) Batentzako ohea Ikasleek hiru egunetan banatutako hainbat jardueratan eman dezakete izena. Ikasgaiak Udako ikastaroak Abizena Telefono zenbakia Adierazi ikasgai zehatz batean interesa duzun. Denborak 27 asteartea Luzeegia Motzegia 25 asteartea Logela mota Beganoa Begetarianoa Webgunea 26 asteazkena Zer gustatu zaizu? Zer hobetu dezakegu? Zer nahi duzue zuen datuekin egitea, proiektua oraingoz onartzen ez badugu? Noiz afaldu nahi duzu? Hotelean egingo dituzu otorduak? Bai, epostaz 3 egun aurretik Bai, epostaz 5 egun aurretik Bai, telefonoz 3 egun aurretik Bai, telefonoz 5 egun aurretik Etenaldiak honelakoak sentitu zenituen: Lehen saioa honelakoa sentitu zenuen: Bigarren saioa honelakoa sentitu zenuen: Kontaktu-pertsonaren informazioa Zure datuak. Zure eposta Zure izena adb. +34 678 655 333 adb. Elkartea, kooperatiba, kolektiboa adb. Hardwarea erosi, soldatak ordaindu etab. adb. FediLiburua adb. Lehen saioa biziki interesgarria izan zen, baina saioa berari buruzko informazio gehiago espero nuen. adb. Datuen babesaren legea betetzea eta hizkuntza-justizia. adb. Bi saioetan gustura egon nintzen. Irakasleak oso gustuko ikasgaia. adb. Jadanik egon naiz zuen jatetxean eta oso atsegina da! adb. Jadanik egona naiz zuen hotelean eta bikaina da! adb. Ongi pasa dut, mila esker. adb. Agur bat besterik ez! adb. Miren adb. Urtxulutegi adb. Espainia adb. Ez zegoen te berderik etenaldian hartzeko. adb. Fedibertsoari buruzko liburu bat idatzi nahi dugu. adb. https://fedi.eus adb. miren@adibidea.eus adb. miren@adibidea.eus 5.000€ arte 