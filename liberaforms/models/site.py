"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, markdown, shutil
import json
from datetime import datetime, timezone
from urllib.parse import urlparse
from PIL import Image
from flask import current_app

from liberaforms import db
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, TIMESTAMP
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm.attributes import flag_modified
from liberaforms.utils.database import CRUD
from liberaforms.utils.consent_mixin import ConsentMixin
from liberaforms.utils import sanitizers
from liberaforms.utils import html_parser
from liberaforms.utils import utils

#from pprint import pprint

class Site(ConsentMixin, db.Model, CRUD):
    """Site model definition."""

    __tablename__ = "site"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    name = db.Column(db.String, nullable=False)
    language = db.Column(db.String, nullable=False)
    primary_color = db.Column(db.String, nullable=False)
    invitation_only = db.Column(db.Boolean, default=True)
    terms_and_conditions = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    smtp_config = db.Column(JSONB, nullable=False)
    newuser_enableuploads = db.Column(db.Boolean, nullable=False, default=False)
    mimetypes = db.Column(JSONB, nullable=False)
    contact_info = db.Column(db.String, nullable=True)
    blurb = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    ldap_filter = db.Column(db.String, nullable=True)
    new_form_msg = db.Column(MutableDict.as_mutable(JSONB), nullable=False)


    def __init__(self):
        """Create a new Site object."""
        self.created = datetime.now(timezone.utc)
        self.name = "LiberaForms!"
        self.language = os.environ['DEFAULT_LANGUAGE']
        self.primary_color = "#b71c1c"
        terms = "Please accept our terms and conditions."
        self.terms_and_conditions = {
                "markdown": terms,
                "html": f"<p>{terms}</p>",
                "checkbox_text": "",
                "enabled": False,
                "required": True
        }
        self.mimetypes = {
                "extensions": ["pdf", "png", "odt"],
                "mimetypes": ["application/pdf",
                              "image/png",
                              "application/vnd.oasis.opendocument.text"]
        }
        self.new_form_msg = {}
        hostname = urlparse(current_app.config['BASE_URL']).netloc
        self.smtp_config = {
                "host": f"smtp.{hostname}",
                "port": 25,
                "encryption": "",
                "user": "",
                "password": "",
                "noreplyAddress": f"no-reply@{hostname}"
        }
        blurb = os.path.join(current_app.root_path, '../assets/front-page.md')
        with open(blurb, 'r') as default_blurb:
            blurb_markdown = default_blurb.read()
        self.blurb = {
            'markdown': blurb_markdown,
            'html': markdown.markdown(blurb_markdown)
        }
        self.set_short_description()

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls):
        """Return this installation's Site object.

        Create the Site object when not present in database.
        """
        site = cls.query.first()
        if not site:
            site = Site()
            site.save()
        return site

    @property
    def hostname(self):
        return urlparse(current_app.config['BASE_URL']).netloc

    @property
    def host_url(self):
        """Return this site's base url."""
        return f"{current_app.config['BASE_URL']}/"

    def change_favicon(self, file):
        """Change Site's icon.

        Save logo.png and favicon.ico
        """
        brand_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                                 current_app.config['BRAND_DIR'])
        # Save the original image as logo.png. # used by opengraph
        new_logo = Image.open(file)
        new_logo.save(os.path.join(brand_dir, 'logo.png'))
        # Convert file to .ico and make the it square
        img = Image.open(file)
        x, y = img.size
        size = max(32, x, y)
        #icon_sizes = [(16,16), (32, 32), (48, 48), (64,64)]
        new_favicon = Image.new('RGBA', (size, size), (0, 0, 0, 0))
        new_favicon.paste(img, (int((size - x) / 2), int((size - y) / 2)))
        new_favicon.save(os.path.join(brand_dir, 'favicon.ico'))

    def reset_favicon(self) -> bool:
        """Reset Site's logo and favicon to default.

        Return True on success
        """
        brand_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                                 current_app.config['BRAND_DIR'])
        logo_path =os.path.join(brand_dir, 'logo.png')
        default_logo =os.path.join(brand_dir, 'logo-default.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        default_favicon = os.path.join(brand_dir, 'favicon-default.ico')
        try:
            shutil.copyfile(default_logo, logo_path)
            shutil.copyfile(default_favicon, favicon_path)
            return True
        except Exception as error:
            current_app.logger.error(error)
            return False

    def get_logo_uri(self) -> str:
        """Return public URL."""
        return f"{self.host_url}logo.png"

    def save_blurb(self, md_text:str) -> None:
        """Save markdown and HTML."""
        self.blurb = {'markdown': sanitizers.remove_html_tags(md_text),
                      'html': sanitizers.markdown2HTML(md_text)}
        self.set_short_description()
        self.save()

    def set_short_description(self) -> None:
        """Create a short text from the blurb."""
        text = html_parser.get_opengraph_text(self.blurb['html'])
        self.blurb['short_text'] = text

    def get_short_description(self) -> str:
        """Return a short text.

        Create and save short text if missing.
        """
        if 'short_text' in self.blurb:
            return self.blurb['short_text']
        self.set_short_description()
        self.save()
        return self.blurb['short_text']

    def get_contact_info(self) -> str:
        return self.contact_info.replace('\n', '<br />') if self.contact_info else ""

    def get_new_form_msg(self, lang_code:str=None) -> str:
        if lang_code:
            if lang_code in self.new_form_msg:
                return sanitizers.bleach_text(self.new_form_msg[lang_code])
            return ""
        current_language = utils.get_locale()
        if current_language in self.new_form_msg and self.new_form_msg[current_language]:
            return sanitizers.bleach_text(self.new_form_msg[current_language])
        for language in self.new_form_msg:
            if self.new_form_msg[language] and self.new_form_msg[language]:
                return sanitizers.bleach_text(self.new_form_msg[language])
        return ""

    def save_terms_and_conditions(self, **kwargs):
        self.terms_and_conditions = {**self.terms_and_conditions, **kwargs}
        self.save()

    def toggle_terms_and_conditions(self) -> bool:
        self.terms_and_conditions['enabled'] = not self.terms_and_conditions['enabled']
        self.save()
        return self.terms_and_conditions['enabled']

    def save_smtp_config(self, **kwargs):
        """Save SMTP configuration."""
        self.smtp_config=kwargs
        self.save()

    def toggle_invitation_only(self) -> bool:
        """Enable/disable open new user registration."""
        self.invitation_only = not self.invitation_only
        self.save()
        return self.invitation_only

    def toggle_newuser_uploads_default(self) -> bool:
        """Enable/disable uploads for new Users."""
        self.newuser_enableuploads = not self.newuser_enableuploads
        self.save()
        return self.newuser_enableuploads

    @staticmethod
    def get_liberaforms_docs_language(lang) -> str:
        """Return an avaiable docs.liberaforms.org language."""
        return lang if lang in ['en'] else "en"

    def get_ldap_filter(self) -> str:
        return self.ldap_filter if self.ldap_filter else current_app.config['LDAP_FILTER']
