"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import shutil
from datetime import datetime, timezone
from sqlalchemy import and_
from sqlalchemy.dialects.postgresql import ARRAY, JSONB, TIMESTAMP, ENUM, UUID
from sqlalchemy.ext.mutable import MutableDict
from flask import current_app, g
from liberaforms import db
from liberaforms.utils.database import CRUD
from liberaforms.models.site import Site
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.models.media import Media
from liberaforms.utils.storage.remote import RemoteStorage
from liberaforms.utils import validators
from liberaforms.utils import crypto
from liberaforms.utils import utils
from liberaforms.utils import tokens


#from pprint import pprint

class User(db.Model, CRUD):
    """User model definition."""

    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password_hash = db.Column(db.String, nullable=False)
    role = db.Column(ENUM("guest", "editor", "admin", name='roles_enum'), nullable=False)
    preferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    blocked = db.Column(db.Boolean, default=False)
    admin = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    validated_email = db.Column(db.Boolean, default=False)
    uploads_enabled = db.Column(db.Boolean, default=False, nullable=False)
    uploads_limit = db.Column(db.Integer, nullable=False)
    token = db.Column(JSONB, nullable=True)
    authored_forms = db.relationship("Form", cascade = "all, delete, delete-orphan")
    timezone = db.Column(db.String, nullable=True)
    alerts = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    fedi_auth = db.Column(MutableDict.as_mutable(JSONB), nullable=True)
    invited_by_id = db.Column(db.Integer, nullable=True)
    ldap_uuid = db.Column(UUID, nullable=True) # user's entry id on the LDAP providor
    media = db.relationship("Media",
                            lazy='dynamic',
                            cascade = "all, delete, delete-orphan")

    def __init__(self, **kwargs):
        """Use to create a new User object."""
        self.created = datetime.now(timezone.utc)
        self.username = kwargs["username"]
        self.email = kwargs["email"]
        self.password_hash = validators.hash_password(kwargs["password"])
        self.role = kwargs["role"] if 'role' in kwargs else 'editor'
        self.preferences = self.default_user_preferences()
        self.blocked = False
        self.admin = self.default_admin_settings()
        self.validated_email = kwargs["validated_email"]
        self.uploads_enabled = Site.find().newuser_enableuploads
        self.uploads_limit = utils.string_to_bytes(current_app.config['DEFAULT_USER_UPLOADS_LIMIT'])
        self.invited_by_id = kwargs["invited_by_id"] if 'invited_by_id' in kwargs else None
        self.ldap_uuid = kwargs["ldap_uuid"] if 'ldap_uuid' in kwargs else None
        self.alerts = {}
        self.token = {}

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first User filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Users filtered by kwargs."""
        filters = []
        if 'token' in kwargs:
            filters.append(cls.token.contains({'token':kwargs['token']}))
            kwargs.pop('token')
        if 'notifyNewForm' in kwargs:
            filters.append(cls.admin.contains({
                                "notifyNewForm": kwargs['notifyNewForm']
                            }))
            kwargs.pop('notifyNewForm')
        if 'notifyNewUser' in kwargs:
            filters.append(cls.admin.contains({
                                "notifyNewUser": kwargs['notifyNewUser']
                            }))
            kwargs.pop('notifyNewUser')
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        return cls.query.filter(*filters)

    def get_id(self):
        """Return id for LoginManager."""
        return self.id

    def is_authenticated(self):
        """LoginManager function."""
        return True

    def is_active(self):
        """LoginManager function."""
        return not self.blocked

    def is_anonymous(self):
        """LoginManager function."""
        return False

    def is_guest(self) -> bool:
        """Check User role."""
        return bool(self.role == 'guest')

    def is_editor(self) -> bool:
        """Check User role."""
        return bool(self.role == 'editor' or self.is_admin())

    def is_admin(self) -> bool:
        """Check User role."""
        return bool(self.role == "admin" or self.is_root_user())

    def is_root_user(self) -> bool:
        """Check User role."""
        return bool(self.email == current_app.config['ROOT_USER'])

    def set_role(self, role:str) -> bool:
        """Set User's role."""
        if self.is_root_user():
            return False
        self.role = role
        return True

    def is_ldap_user(self):
        return bool(self.ldap_uuid)

    @classmethod
    def count(cls) -> int:
        """Return total of all Users."""
        return cls.query.count()

    def get_created_date(self) -> str:
        """Return the User's creation date."""
        return utils.utc_to_g_timezone(self.created).strftime("%Y-%m-%d")

    @property
    def enabled(self) -> bool:
        """Is this User enabled."""
        if not self.validated_email:
            return False
        if self.blocked:
            return False
        return True

    def get_timezone(self) -> str:
        """Return this User's timezone."""
        if self.timezone:
            return self.timezone
        return current_app.config['DEFAULT_TIMEZONE']

    def get_form(self, form_id, **kwargs):
        """Return a Form with form_id."""
        kwargs = {**{'form_id':form_id}, **kwargs}
        return self.get_forms(**kwargs).first()

    def get_forms(self, **kwargs):
        """Return filtered Forms."""
        kwargs = {**{'user_id': self.id}, **kwargs}
        return Form.query.join(FormUser, Form.id == FormUser.form_id) \
                         .filter_by(**kwargs)

    def authored_forms_total(self, **kwargs) -> int:
        """Return this Use'rs total authored Forms."""
        kwargs["author_id"] = self.id
        return Form.find_all(**kwargs).count()

    def can_inspect_form(self, form) -> bool:
        """Can User inspect Form."""
        if self.is_admin():
            return True
        if FormUser.find(user_id=self.id, form_id=form.id, is_editor=True):
            return True
        return False

    def invited_by(self):
        """Return User object that invited this User.

        Returns None when user created an account (site.invitation_only=False)
        """
        if not self.invited_by_id:
            return None
        user = User.find(id=self.invited_by_id)
        return user if user else None

    @property
    def language(self) -> str:
        """Return User's preferred language."""
        return self.preferences["language"]

    def new_form_notifications(self) -> dict:
        """Use to define new FormUser preference."""
        return {'newAnswer': self.preferences["newAnswerNotification"],
                'expiredForm': True}

    def new_answer_notification_default(self) -> bool:
        """User preference for new Forms."""
        return self.preferences["newAnswerNotification"]

    def get_media_dir(self) -> str:
        """Return the os path when this User's media is stored."""
        return os.path.join(current_app.config['UPLOADS_DIR'],
                            current_app.config['MEDIA_DIR'],
                            str(self.id))

    def media_usage(self) -> int:
        """Total Nedia disk usage."""
        return Media.calc_total_size(user_id=self.id)

    def attachments_usage(self) -> int:
        """Total AnswerAttachment disk usage."""
        return AnswerAttachment.calc_total_size(author_id=self.id)

    def total_uploads_usage(self) -> int:
        """Total disk usage."""
        return self.media_usage() + self.attachments_usage()

    def can_enable_uploads(self) -> bool:
        """Can uploads be enbled for this User."""
        if self.role == 'guest':
            return False
        if not (current_app.config['ENABLE_UPLOADS'] and current_app.config['CRYPTO_KEY']):
            return False
        if Media.calc_total_size() + AnswerAttachment.calc_total_size() > \
           utils.string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT']):
            # site-wide limit exceeded
            return False
        return self.total_uploads_usage() < self.uploads_limit

    def can_upload(self) -> bool:
        """Can this User upload Media and solicit AnswerAttachment."""
        if not self.can_enable_uploads():
            return False
        return self.uploads_enabled

    def toggle_uploads_enabled(self) -> bool:
        """Enable/disable uploads."""
        if not self.can_enable_uploads():
            return False
        self.uploads_enabled = not self.uploads_enabled
        self.save()
        return self.uploads_enabled

    def set_uploads_limit(self, limit:int) -> None:
        max_limit = utils.string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT'])
        self.uploads_limit = limit if limit < max_limit else max_limit

    def set_disk_usage_alert(self) -> bool:
        """Alert the User that uploads limits has been surpassed.

        Return True when alert value changes.
        """
        alert = self.total_uploads_usage() > self.uploads_limit
        if alert and 'disk_usage' not in self.alerts:
            self.alerts['disk_usage'] = True
            return True
        if not alert and 'disk_usage' in self.alerts:
            del self.alerts['disk_usage']
            return True
        return False

    def update_alerts(self) -> bool:
        """Update User's alerts."""
        return self.set_disk_usage_alert()

    def delete_user(self) -> None:
        """Delete User, Forms, Media."""
        # remove this user from FormUser
        for formuser in FormUser.find_all(user_id=self.id):
            if formuser.form.author_id != self.id:
                formuser.delete()
        # delete uploaded media files
        shutil.rmtree(self.get_media_dir(), ignore_errors=True)
        if current_app.config['ENABLE_REMOTE_STORAGE'] is True:
            prefix = f"media/{self.id}"
            RemoteStorage().remove_directory(prefix)
        attachment_dirs = [form.get_attachment_dir() for form in self.authored_forms]
        for attachment_dir in attachment_dirs:
            shutil.rmtree(attachment_dir, ignore_errors=True)
        # cascade delete user.authored_forms
        # cascade delete user.media
        self.delete()

    def save_token(self, **kwargs) -> None:
        """Use to recover password and to validate email address."""
        self.token=tokens.create_token(User, **kwargs)
        self.save()

    def delete_token(self) -> None:
        """Delete the token."""
        self.token={}
        self.save()

    def toggle_blocked(self) -> bool:
        """Enable/disable this User."""
        if self.is_root_user():
            self.blocked=False
        else:
            self.blocked= not self.blocked
        self.save()
        return self.blocked

    def toggle_new_answer_notification_default(self):
        """Toggle notification preference."""
        preference = self.preferences["newAnswerNotification"]
        self.preferences["newAnswerNotification"] = not preference
        self.save()
        return self.preferences["newAnswerNotification"]

    @staticmethod
    def default_user_preferences() -> dict:
        """Set new User's preferences."""
        return {
            "language": Site.find().language,
            "newAnswerNotification": True,
            "show_edit_alert": True
        }

    @staticmethod
    def default_admin_settings() -> dict:
        """Define default Admin preferences."""
        return {
            "notifyNewUser": False,
            "notifyNewForm": False,
            "forms": {},
            "users": {},
            "userforms": {}
        }

    def get_fedi_auth(self) -> dict:
        """Return User's Fediverse authorization."""
        if not self.fedi_auth:
            return {"node_url": "", "access_token": ""}
        return crypto.decrypt_dict(self.fedi_auth)

    def set_fedi_auth(self, data:dict) -> None:
        """Set User's Fediverse authorization."""
        self.fedi_auth = crypto.encrypt_dict(data)

    def fedi_connection_title(self) -> str:
        """Return the title of the authorization."""
        auth = self.get_fedi_auth()
        return auth['title'] if 'title' in auth else ""

    def toggle_new_user_notification(self) -> bool:
        """Send this admin an email when a new user registers at the site."""
        if not self.is_admin():
            return False
        self.admin['notifyNewUser'] = not self.admin['notifyNewUser']
        self.save()
        return self.admin['notifyNewUser']

    def toggle_new_form_notification(self) -> bool:
        """Send this admin an email when a new form is created."""
        if not self.is_admin():
            return False
        self.admin['notifyNewForm'] = not self.admin['notifyNewForm']
        self.save()
        return self.admin['notifyNewForm']

    def get_answers(self, **kwargs):
        """Return Answers to Forms authored by this User."""
        kwargs['author_id']=str(self.id)
        return Answer.find_all(**kwargs)
