"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pathlib
from datetime import datetime, timezone
from PIL import Image
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP
from sqlalchemy import func
from flask import current_app, g
from liberaforms import db
from liberaforms.utils.storage.storage import Storage
from liberaforms.utils.database import CRUD
from liberaforms.utils import utils

import sqlalchemy
from sqlalchemy.sql.expression import cast

#from pprint import pprint as pp


class Media(db.Model, CRUD, Storage):
    """Media model definition."""

    __tablename__ = "media"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                  ondelete="CASCADE"),
                                                  nullable=False)
    alt_text = db.Column(db.String, nullable=True)
    file_name = db.Column(db.String, nullable=False)
    file_size = db.Column(db.Integer, nullable=False)
    storage_name = db.Column(db.String, nullable=False)
    local_filesystem = db.Column(db.Boolean, default=True) #Remote storage = False
    user = db.relationship("User", viewonly=True)

    def __init__(self):
        """Create a new Media object."""
        Storage.__init__(self)
        self.created = datetime.now(timezone.utc)
        self.encrypted = False

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first Media filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Medias filtered by kwargs."""
        return cls.query.filter_by(**kwargs)

    @classmethod
    def calc_total_size(cls, **kwargs) -> int:
        """Total Nedia disk usage filtered by kwargs."""
        filters = []
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        total = cls.query.filter(*filters).with_entities(
                func.sum(cls.file_size.cast(sqlalchemy.Integer))
            ).scalar()
        return total if total else 0

    @property
    def directory(self) -> str:
        """Return os directory path to Media."""
        return f"{current_app.config['MEDIA_DIR']}/{self.user_id}"

    def save_media(self, user, file, alt_text):
        """Save Media and thumbnail."""
        self.user_id = user.id
        self.alt_text = alt_text
        self.file_name = file.filename
        extension = pathlib.Path(self.file_name).suffix
        self.storage_name = f"{utils.gen_random_string()}{extension}"
        saved = super().save_file(file, self.storage_name, self.directory)
        if saved:
            self.save()
            self.save_thumbnail()
        return saved

    def delete_media(self) -> bool:
        """Delete Media and thumbnail.

        Return True on success.
        """
        Storage.__init__(self)
        removed_media = super().delete_file(self.storage_name, self.directory)
        removed_thumbnail = self.delete_thumbnail()
        self.delete()
        if removed_media and removed_thumbnail:
            return True
        return False

    def delete_thumbnail(self) -> bool:
        """Delete Media's thumbnail.

        Return True on success
        """
        storage_name = f"tn-{self.storage_name}"
        return super().delete_file(storage_name, self.directory)

    def _get_media_url(self, storage_name) -> str:
        """Return file URL."""
        if self.local_filesystem:
            host_url = current_app.config['BASE_URL']
            return f"{host_url}/file/media/{self.user_id}/{storage_name}"
        # creates a URL for the media file on the minio server
        bucket_name = f"{g.site.hostname}.media"
        media_path = f"{bucket_name}/{self.user_id}/{storage_name}"
        return f"{os.environ['MINIO_URL']}/{media_path}"

    def get_url(self) -> str:
        """Return Media's URL."""
        return self._get_media_url(self.storage_name)

    def get_thumbnail_url(self) -> str:
        """Return Media's thumbnail URL."""
        return self._get_media_url(f"tn-{self.storage_name}")

    def get_media(self) -> tuple:
        """Return file byte stream and file name."""
        file_bytes = super().get_file(self.storage_name, self.directory)
        return file_bytes, self.file_name

    def does_media_exits(self, thumbnail=False) -> bool:
        """Check if file exists."""
        name = self.storage_name if thumbnail is False else f"tn-{self.storage_name}"
        return super().does_file_exist(self.directory, name)

    def save_thumbnail(self):
        """Compress Media and save as thumbnail."""
        storage_name = f"tn-{self.storage_name}"
        tmp_thumbnail_path = os.path.join(current_app.config['TMP_DIR'],
                                          storage_name)
        (stream, file_name) = self.get_media()
        with open(tmp_thumbnail_path, "wb") as outfile:
            #Copy the BytesIO stream to the output file
            outfile.write(stream.getbuffer())
        image = Image.open(tmp_thumbnail_path)
        image.thumbnail((50,50))
        image.save(tmp_thumbnail_path)
        storage = Storage()
        storage.save_file(tmp_thumbnail_path, storage_name, self.directory)
        #except Exception as error:
        #    current_app.logger.warning(f"Could not create thumbnail: {error}")


#@event.listens_for(Media, "after_delete")
#def delete_user_media(mapper, connection, target):
#    deleted = target.delete_media()
