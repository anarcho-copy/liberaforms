"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask_babel import gettext as _
from liberaforms import ma
from liberaforms.models.consent import Consent
from liberaforms.utils import utils


class ConsentSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Consent

    id = ma.Integer()
    created = ma.Method('get_created')
    title = ma.auto_field()
    markdown = ma.auto_field()
    html = ma.auto_field()
    #html = ma.Method('get_html')
    checkbox_text = ma.Method('get_checkbox_text')
    required = ma.auto_field()
    enabled = ma.auto_field()
    shared = ma.auto_field()

    def get_created(self, obj) -> str:
        return utils.utc_to_g_timezone(obj.created).strftime("%Y-%m-%d %H:%M:%S")

    def get_checkbox_text(self, obj) -> str:
        if not obj.checkbox_text:
            return _("I agree")
        return obj.checkbox_text

    #def get_html(self, obj) -> str:
    #    print('shema.html: ', obj.html)
    #    return obj.html
