# LiberaForms

https://liberaforms.org

See the `/docs` directory for installation and setup documentation.

## What is LiberaForms

LiberaForms is an easy to use software for creating and managing simple web Forms and
their Answers (collected data).

This software is licensed under the AGPLv3. We hope you find it useful.

### User features include

* Easy to use Form creation (usual form elements. input, textarea, date, etc.)
* Solicit file attachments
* Email notifications
* Form expiration conditionals
* Editable 'Thank you for submitting' texts
* Editable 'This form has expired' texts
* Builtin GDPR option and templating
* Export answers in Graphs, CSV, JSON, PDF
* Share Form edition with other users
* Share Answers (read-only) with other users
* Usage statistics
* Publish Forms on the Fediverse
* Form templates

### Admin features include

* Site configuration parameters
* List all Forms and view Form properties.
* List all Users and edit User parameters.
* Per user and site-wide usage statistics

Complete list of features and user/admin documentation at https://docs.liberaforms.org

### Sysadmin

Keywords. `Python` `Flask` `PostgreSQL` `Nginx` `VueJS` `RSS` `Docker` `Prometheus` `LDAP`

See `./docs` for more info. :)
