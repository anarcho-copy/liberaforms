"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests.assets import form_structure
from tests.utils import random_slug

class TestPublicFormSubmit():

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor, expiry_conditions):
        self.properties['editor']=editor
        form=FormFactory(author=editor,
                         slug=random_slug())
        form.save()
        self.properties['number_field_name']=form.get_number_fields()[0]['name']
        #assert self.number_field_name == "number-1620224716308"
        assert self.properties['number_field_name'] in form_structure.get_form_structure()
        self.properties['form']=form

        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()


    def test_display_form(self, anon_client):
        # test without client login. aka anonymous user
        self.properties['form'].restricted_access = False
        self.properties['form'].enabled = False
        self.properties['form'].save()
        form_url = self.properties['form'].url
        #print(form_url)
        # test disabled form
        response = anon_client.get(
                        form_url,
                        follow_redirects=True,
                    )
        assert response.status_code == 404
        self.properties['form'].enabled = True
        self.properties['form'].save()
        # test enabled form
        response = anon_client.get(
                        form_url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<meta name="robots" content="noindex">' in html
        assert '"label": "Name", "name": "text-1620232883208"' in html

    def test_submit_form(self, anon_client):
        form_url = self.properties['form'].url
        name = "Julia"
        response = anon_client.post(
                        form_url,
                        data = {
                            "text-1620232883208": name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        assert self.properties['form'].get_after_submit_text_html() in html
        answer = self.properties['form'].answers[-1]
        assert vars(answer)['data']['text-1620232883208'] == name
        assert vars(answer)['marked'] == False

    def test_sumbit_embedded_form(self, anon_client):
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        embedded_form_url = self.properties['form'].get_embed_url()
        response = anon_client.get(embedded_form_url)
        assert response.status_code == 200
        html = response.data.decode()
        assert '"label": "Name", "name": "text-1620232883208"' in html
        name = "Stella"
        response = anon_client.post(
                        embedded_form_url,
                        data = {
                            "text-1620232883208": name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        assert self.properties['form'].get_after_submit_text_html() in html
        answer = self.properties['form'].answers[-1]
        assert vars(answer)['data']['text-1620232883208'] == name
        os.environ['SKIP_EMAILS'] = original_skip_emails


    def test_max_answers_expiration(self, anon_client, expiry_conditions):
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        form = self.properties['form']
        form.expiry_conditions={
            "fields": {},
            "totalAnswers": 10,
            "expireDate": None
        }
        form.save()
        max_answers = expiry_conditions['max_answers']
        assert form.answers.count() < max_answers
        name = "Julia"
        while form.answers.count() < max_answers:
            assert form.expired == False
            response = anon_client.post(
                            form.url,
                            data = {
                                "text-1620232883208": name,
                            },
                            follow_redirects=True,
                        )
            assert response.status_code == 200
            html = response.data.decode()
            assert "<!-- thank_you_page -->" in html
        assert form.answers.count() == max_answers
        assert form.expired == True
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- form_has_expired_page -->" in html
        os.environ['SKIP_EMAILS'] = original_skip_emails

    def test_max_number_field_expiration(self, anon_client, expiry_conditions):
        """Post the form 4 times and check expiry."""
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        form = self.properties['form']
        form.expiry_conditions={
            "fields": {
                self.properties['number_field_name']: {
                    "type": "number",
                    "condition": expiry_conditions['number_field_max']
                }
            },
            "expireDate": False,
            "totalAnswers": 0
        }
        form.expired=False
        form.save()
        number_to_submit = 2
        names = ["Stella", "Debbie", "Jackie"]
        # the form will not exipire after three submits because:
        number_field_max = expiry_conditions['number_field_max']
        assert len(names) * number_to_submit < number_field_max
        number_field_name = self.properties['number_field_name']
        for name in names:
            response = anon_client.post(
                            form.url,
                            data = {
                                "text-1620232883208": name,
                                number_field_name: number_to_submit
                            },
                            follow_redirects=True,
                        )
            assert response.status_code == 200
            html = response.data.decode()
            assert "<!-- thank_you_page -->" in html
            answer = self.properties['form'].answers[-1]
            assert vars(answer)['data']['text-1620232883208'] == name
            assert form.tally_number_field(number_field_name) < number_field_max
            assert form.expired is False
            assert vars(answer)['marked'] is False
        # the form should exipire
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": "Vicky",
                            number_field_name: number_to_submit
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        assert form.expired == True
        os.environ['SKIP_EMAILS'] = original_skip_emails

    def test_expired_form(self, anon_client):
        form = self.properties['form']
        assert form.expired == True
        name = "Rita"
        response = anon_client.post(
                        form.url,
                        data = {
                            "text-1620232883208": name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- form_has_expired_page -->" in html
        assert form.get_expired_text_html() in html
