"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import werkzeug
from io import BytesIO
from tests.utils import login, logout


class TestSiteStatistics():
    def test_site_stats_graph(cls, users, anon_client, editor_client, admin_client):
        """ Tests site statistics page
            Only tests if the page was generated
            Tests permissions
        """
        url = "/site/stats"
        login(editor_client, users['editor'])
        login(admin_client, users['admin'])
        response = anon_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html

        response = editor_client.get(
                            url,
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        response = admin_client.get(
                        url,
                        follow_redirects=True
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_stats_page -->' in html
