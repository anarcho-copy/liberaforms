"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
import flask_login
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormQR():

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_qr_page(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('form_bp.form_qr', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- form_qr_page -->" in html

    def test_qr_download(self, editor_client):
        form = self.properties['form']
        response = editor_client.get(
                        url_for('form_bp.form_qr', form_id=form.id, as_png=True),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.mimetype == 'image/png'
        assert 'x89PNG' in str(response.data)
