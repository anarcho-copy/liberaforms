"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from liberaforms.models.user import User
from liberaforms.models.media import Media
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from tests.factories import FormFactory, UserFactory
from tests.assets.form_structure import get_form_structure
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout, random_slug

class TestDeleteUser():
    """Delete a user with form, attachments, and media.

    Creates 5 answers with attachments and 1 media upload
    Tests the deletion of the user, media, forms and answers and attachments.
    """

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.properties={}
        cls.user = UserFactory(username='guinea', validated_email=True)
        cls.user.save()

    def test_requirements(self, editor_client, anon_client):
        """Create a form and answers with attachment."""
        form=FormFactory(author=self.user,
                         slug=random_slug(),
                         structure=json.loads(get_form_structure(with_attachment=True)))
        form.enabled = True
        form.save()
        form_user = FormUser(
                user_id = self.user.id,
                form_id = form.id,
                is_editor = True,
                notifications = self.user.new_form_notifications()
        )
        form_user.save()
        self.properties['form']=form

        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        total_answers=10
        name="Julia"
        answer_cnt = 0
        # create answers and attachments
        while answer_cnt < total_answers:
            answer_cnt = answer_cnt +1
            with open(valid_attachment_path, 'rb') as f:
                stream = BytesIO(f.read())
            valid_file = werkzeug.datastructures.FileStorage(
                stream=stream,
                filename=valid_attachment_name,
                content_type=mimetype,
            )
            anon_client.post(
                form.url,
                data = {
                    "text-1620232883208": name,
                    "file-1622045746136": valid_file,
                },
                follow_redirects=True,
            )
        assert form.answers.count() == total_answers
        assert len(os.listdir(form.get_attachment_dir())) == total_answers
        # upload media
        login(editor_client, {'username': self.user.username, 'password': VALID_PASSWORD})
        url = "/media/save"
        valid_media_name = "valid_media.png"
        valid_media_path = f"./assets/{valid_media_name}"
        with open(valid_media_path, 'rb') as file:
            response = editor_client.post(
                            url,
                            data = {
                                'media_file': file,
                                'alt_text': "valid alternative text",
                            },
                            follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        # both 1. the uploaded media file and 2. the thumbnail
        assert len(os.listdir(self.user.get_media_dir())) == 2
        #logout(editor_client)

    def test_delete_user(self, admin_client):
        """Delete user."""
        delete_user_url = f"/admin/user/{self.user.id}/delete"
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        delete_user_url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- admin_delete_user_page -->' in html
        attachment_dirs = [form.get_attachment_dir() for form in self.user.authored_forms]
        response = admin_client.post(
                        delete_user_url,
                        data = {
                            "username": self.user.username,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert FormUser.find_all(user_id=self.user.id).count() == 0
        assert Form.find_all(author_id=self.user.id).count() == 0
        assert Answer.find_all(author_id=self.user.id).count() == 0
        assert '<!-- list_users_page -->' in html
        assert os.path.isdir(self.user.get_media_dir()) is False
        assert Media.find(user_id=self.user.id) is None
        assert User.find(id=self.user.id) is None
        for attachment_dir in attachment_dirs:
            assert os.path.isdir(attachment_dir) is False
