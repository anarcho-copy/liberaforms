"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import flask_login
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormExpirationSettings():

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_toggle_expiration_notification(self, editor_client):
        form_id = self.properties['form'].id
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        f"/form/{form_id}/expiration",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- form_expiration_page -->" in html
        form_user=FormUser.find(form_id=form_id,
                                user_id=flask_login.current_user.id)
        initial_preference = form_user.notifications['expiredForm']
        response = editor_client.post(
                        f"/form/{form_id}/expiration/toggle-notification",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['notification'] != initial_preference
        assert initial_preference != form_user.notifications['expiredForm']

    def test_set_expiration_date(self, anon_client, editor_client):
        form_id = self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        invalid_date="2021-00-00"
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-date",
                        data = {
                            "date": invalid_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['expired'] == False
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == False
        assert self.properties['form'].log.count() == initial_log_count
        valid_past_date="2021-01-01"
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-date",
                        data = {
                            "date": valid_past_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['expired'] == True
        assert self.properties['form'].has_expired() == True
        assert self.properties['form'].can_expire() == True
        assert self.properties['form'].log.count() == initial_log_count +1
        initial_log_count = self.properties['form'].log.count()
        valid_future_date="2121-05-05"
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-date",
                        data = {
                            "date": valid_future_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['expired'] == False
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == True
        assert self.properties['form'].log.count() == initial_log_count +1
        initial_log_count = self.properties['form'].log.count()
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-date",
                        data = {
                            "date": "",
                            "time": ""
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['expired'] == False
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == False
        assert self.properties['form'].log.count() == initial_log_count +1

    def test_set_max_answers_expiration(self, editor_client, expiry_conditions):
        """ Tests valid max answers exipry condition value
            Tests invalid max answers exipry condition value
        """
        form_id=self.properties['form'].id
        initial_log_count = self.properties['form'].log.count()
        valid_max_answers = expiry_conditions['max_answers']
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-total-answers",
                        data = {
                            "total_answers": valid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['total_answers'] == valid_max_answers
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == True
        assert self.properties['form'].log.count() == initial_log_count +1
        initial_log_count = self.properties['form'].log.count()
        invalid_max_answers = "invalid_integer"
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-total-answers",
                        data = {
                            "total_answers": invalid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['total_answers'] == 0
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == False
        assert self.properties['form'].expiry_conditions['totalAnswers'] == 0
        assert self.properties['form'].log.count() == initial_log_count +1
        initial_log_count = self.properties['form'].log.count()
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-total-answers",
                        data = {
                            "total_answers": invalid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == False
        assert self.properties['form'].log.count() == initial_log_count +1

    # ./tests/assets/valid_form_structure.json contains a number field
    # with id number-1620224716308
    def test_set_max_number_field_expiration(self, editor_client, expiry_conditions):
        """ Tests for max_number_field input in html
            Tests valid max answers exipry condition value
            Tests invalid max answers exipry condition value
        """
        form_id = self.properties['form'].id
        number_field_id = "number-1620224716308"
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        f"/form/{form_id}/expiration",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'<input    id="{number_field_id}" type="number"' in html
        url = f"/form/{form_id}/expiration/set-field-condition"
        initial_log_count = self.properties['form'].log.count()
        valid_max_number = expiry_conditions['number_field_max']
        response = editor_client.post(
                        url,
                        data = {
                            "field_name": number_field_id,
                            "condition": valid_max_number
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['condition'] == str(valid_max_number)
        assert response.json['expired'] is False
        assert self.properties['form'].has_expired() is False
        assert self.properties['form'].can_expire() is True
        field_condition = {'type': 'number', 'condition': expiry_conditions['number_field_max']}
        assert self.properties['form'].expiry_conditions['fields'][number_field_id] == field_condition
        assert self.properties['form'].log.count() == initial_log_count + 1
        initial_log_count = self.properties['form'].log.count()
        invalid_max_number = "invalid_integer"
        response = editor_client.post(
                        url,
                        data = {
                            "field_name": number_field_id,
                            "condition": invalid_max_number
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['condition'] == 0
        assert response.json['expired'] == False
        assert self.properties['form'].has_expired() == False
        assert self.properties['form'].can_expire() == False
        assert self.properties['form'].expiry_conditions['fields'] == {}
        assert self.properties['form'].log.count() == initial_log_count + 1

    def test_set_expiration_settings(self, editor_client, expiry_conditions):
        """ Set up expiration conditions for submit tests to be made later
            No assertions are made in this function. (previously tested)
            This is the last function in this module.
        """
        form_id = self.properties['form'].id
        number_field_id = "number-1620224716308"
        valid_max_answers = expiry_conditions['max_answers']
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-total-answers",
                        data = {
                            "total_answers": valid_max_answers,
                        },
                        follow_redirects=False,
                    )
        valid_max_number = expiry_conditions['number_field_max']
        response = editor_client.post(
                        f"/form/{form_id}/expiration/set-field-condition",
                        data = {
                            "field_name": number_field_id,
                            "condition": valid_max_number
                        },
                        follow_redirects=False,
                    )
