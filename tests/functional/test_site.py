"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from io import BytesIO
import pytest
import werkzeug
from flask import url_for
from liberaforms.models.site import Site
from tests.utils import login
from tests import user_creds
import flask_login

class TestSiteConfig():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_uploads_tree(self, app):
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['MEDIA_DIR'])) is True
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['BRAND_DIR'])) is True
        assert os.path.isdir(os.path.join(app.config['UPLOADS_DIR'],
                                          app.config['ATTACHMENT_DIR'])) is True
        if 'FQDN' in os.environ:
            assert os.environ['FQDN'] in app.config['MEDIA_DIR']
            assert os.environ['FQDN'] in app.config['BRAND_DIR']
            assert os.environ['FQDN'] in app.config['ATTACHMENT_DIR']

    def test_site_default_values(self, app):
        pass

    #@pytest.mark.usefixtures("authenticated_admin")
    def test_change_sitename(self, users, anon_client, editor_client, admin_client):
        login(admin_client, users['admin'])
        url = "/site/change-sitename"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        response = admin_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- change_sitename_page -->' in html
        initial_sitename = self.site.name
        new_name = "New name!!"
        response = admin_client.post(
                        "/site/change-sitename",
                        data = {
                            "sitename": new_name,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.name != initial_sitename

    def test_change_default_language(self, db, admin_client, anon_client):
        """ Tests unavailable language and available language
            as defined in ./liberaforms/config.py
            Tests admin permission
        """
        url = "/site/change-default-language"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        initial_language = self.site.language
        #login(client, users['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        #print(html)
        assert '<!-- change_language_page -->' in html
        """
        if 300 <= response.status_code < 400:
            response = client.get(response.headers['Location'], headers={
                "Referer": url
            })
        """
        unavailable_language = 'af' # Afrikaans
        response = admin_client.post(
                        url,
                        data = {
                            "language": unavailable_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.language == initial_language
        available_language = 'ca'
        response = admin_client.post(
                        url,
                        data = {
                            "language": available_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.language == available_language

    def test_toggle_invitation_only(self, admin_client, anon_client):
        url = "/site/toggle-invitation-only"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        invitation_only = self.site.invitation_only
        #login(client, users['admin'])
        response = admin_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.site.invitation_only != invitation_only

    def test_change_primary_color(self, admin_client, anon_client):
        """ Tests valid and invalid html hex color
            Tests admin permission
        """
        url = "/site/primary-color"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        #login(client, users['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert '<div class="menu-color-options">' in html
        initial_color = self.site.primary_color
        bad_color = "green"
        response = admin_client.post(
                        url,
                        data = {
                            "hex_color": bad_color
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.primary_color == initial_color
        html = response.data.decode()
        assert '<div class="menu-color-options">' in html
        valid_color = "#cccccc"
        response = admin_client.post(
                        url,
                        data = {
                            "hex_color": valid_color
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.site.primary_color != initial_color
        html = response.data.decode()
        assert '<div id="site_settings"' in html

    def test_change_logo(self, app, admin_client, anon_client):
        """ Tests valid and invalid image files in ./tests/assets
            Tests jpeg to png logo conversion
            Tests favicon creation
            Tests admin permission
        """
        url = "/site/change-icon"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        #login(client, users['admin'])
        response = admin_client.get(
                    url,
                    follow_redirects=False,
                )
        html = response.data.decode()
        assert '<!-- change_icon_page -->' in html
        brand_dir = os.path.join(app.config['UPLOADS_DIR'],
                                 app.config['BRAND_DIR'])
        logo_path = os.path.join(brand_dir, 'logo.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        initial_logo_stats = os.stat(logo_path)
        initial_favicon_stats = os.stat(favicon_path)
        invalid_logo = "invalid_logo.jpeg"
        #invalid_favicon = "favicon_invalid.jpeg"
        with open(f'./assets/{invalid_logo}', 'rb') as f:
            stream = BytesIO(f.read())
        invalid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=invalid_logo,
            content_type="plain/txt",
        )
        response = admin_client.post(
                    url,
                    data = {
                        'file': invalid_file,
                    },
                    follow_redirects=True,
                    content_type='multipart/form-data',
                )
        assert response.status_code == 200
        assert initial_logo_stats.st_size == os.stat(logo_path).st_size
        assert initial_favicon_stats.st_size == os.stat(favicon_path).st_size
        html = response.data.decode()
        assert '<!-- change_icon_page -->' in html
        valid_logo = "valid_logo.jpeg"
        with open(f'./assets/{valid_logo}', 'rb') as f:
            stream = BytesIO(f.read())
        valid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_logo,
            content_type="image/png",
        )
        response = admin_client.post(
                    url,
                    data = {
                        'file': valid_file,
                    },
                    follow_redirects=True,
                    content_type='multipart/form-data',
                )
        assert response.status_code == 200
        assert initial_logo_stats.st_size != os.stat(logo_path).st_size
        assert initial_favicon_stats.st_size != os.stat(favicon_path).st_size
        html = response.data.decode()
        assert '<!-- admin-panel_page -->' in html

    def test_restore_default_logo(self, app, admin_client, anon_client):
        url = "/site/reset-favicon"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        brand_dir = os.path.join(app.config['UPLOADS_DIR'],
                                 app.config['BRAND_DIR'])
        logo_path = os.path.join(brand_dir, 'logo.png')
        default_logo_path = os.path.join(brand_dir, 'logo-default.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        default_favicon_path = os.path.join(brand_dir, 'favicon-default.ico')
        initial_logo_stats = os.stat(logo_path)
        initial_favicon_stats = os.stat(favicon_path)
        #login(client, users['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert initial_logo_stats.st_size != os.stat(logo_path).st_size
        assert os.stat(logo_path).st_size == os.stat(default_logo_path).st_size
        assert initial_favicon_stats.st_size != os.stat(favicon_path).st_size
        assert os.stat(favicon_path).st_size == os.stat(default_favicon_path).st_size
        html = response.data.decode()
        assert '<div id="site_settings"' in html

    def test_toggle_newuser_enableuploads(self, admin_client, anon_client):
        """ Tests permissions
            Tests toggle
        """
        url = "/site/toggle-newuser-uploads-default"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        initial_newuser_enableuploads = self.site.newuser_enableuploads
        #login(client, users['admin'])
        response = admin_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json == {"uploads": self.site.newuser_enableuploads}
        assert initial_newuser_enableuploads != self.site.newuser_enableuploads

    def test_edit_blurb(self, admin_client, anon_client):
        """ Posts markdown and tests resulting HTML and short_text
            Tests admin permission
        """
        url = "/site/save-blurb"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        #login(client, users['admin'])
        response = admin_client.post(
                    url,
                    data = {
                        'editor': "# Tested !!\nline1\nline2",
                    },
                    follow_redirects=True,
                )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<h1>Tested !!</h1>' in html
        assert '<p>line1<br />\nline2</p>' in html
        assert '<h1>Tested !!</h1>' in self.site.blurb['html']
        assert '# Tested !!' in self.site.blurb['markdown']
        assert 'Tested !!' in self.site.blurb['short_text']

    def test_save_smtp_config(self, anon_client, editor_client, admin_client):
        """ Tests invalid and valid smtp configuration
            Tests permission
        """
        response = anon_client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=True,
                    )
        assert '<!-- site_index_page -->' in response.data.decode()
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=True,
                    )
        assert '<!-- site_index_page -->' in response.data.decode()
        initial_smtp_config = self.site.smtp_config
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- smtp_config_page -->' in html
        invalid_smtp_conf = {
            'host': 'smtp.example.com',
            'port': "i_am_a_string",
            'encryption': 'SSL',
            'user': 'username',
            'password': 'password',
            'noreplyAddress': 'noreply@example.com'
        }
        response = admin_client.post(
                        url_for('site_bp.smtp_config'),
                        data = invalid_smtp_conf,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert self.site.smtp_config == initial_smtp_config
        assert '<!-- smtp_config_page -->' in response.data.decode()
        valid_smtp_conf = {
            'host': os.environ['TEST_SMTP_HOST'],
            'port': int(os.environ['TEST_SMTP_PORT']),
            'encryption': os.environ['TEST_SMTP_ENCRYPTION'],
            'user': os.environ['TEST_SMTP_USERNAME'],
            'password': os.environ['TEST_SMTP_USER_PASSWORD'],
            'noreplyAddress': os.environ['TEST_SMTP_NO_REPLY']
        }
        response = admin_client.post(
                        url_for('site_bp.smtp_config'),
                        data = valid_smtp_conf,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert self.site.smtp_config['host'] == os.environ['TEST_SMTP_HOST']
        assert self.site.smtp_config['port'] == int(os.environ['TEST_SMTP_PORT'])
        assert self.site.smtp_config['encryption'] == os.environ['TEST_SMTP_ENCRYPTION']
        assert self.site.smtp_config['user'] == os.environ['TEST_SMTP_USERNAME']
        assert self.site.smtp_config['password'] == os.environ['TEST_SMTP_USER_PASSWORD']
        assert self.site.smtp_config['noreplyAddress'] == os.environ['TEST_SMTP_NO_REPLY']

    @pytest.mark.skipif(os.environ['SKIP_EMAILS'] == 'True', reason="SKIP_EMAILS=True in test.ini")
    def test_test_smtp_config(self, admin_client):
        """Send a test email."""
        #login(client, users['admin'])
        response = admin_client.post(
                        "/site/email/test-config",
                        data = {
                            'email': user_creds['admin']['email'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html

    def test_mimetypes(self, admin_client):
        """Set allowed mimetypes."""
        site=Site.find()
        response = admin_client.get(
                        url_for('site_bp.edit_mimetypes'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- edit_mimetypes_page -->' in html
        assert 'jpg' not in site.mimetypes['extensions']
        mimetype_data = site.mimetypes['extensions']
        mimetype_data.append('jpg')
        mimetype_data = '\n'.join(mimetype_data)
        response = admin_client.post(
                        url_for('site_bp.edit_mimetypes'),
                        data = {
                            'extensions': mimetype_data,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert 'image/jpeg' in site.mimetypes['mimetypes']
        assert 'jpg' in site.mimetypes['extensions']
