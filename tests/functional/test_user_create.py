"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import flask_login
from flask import current_app
from liberaforms.commands.user import create as create_user
from liberaforms.models.site import Site
from liberaforms.models.user import User
from factories import UserFactory
from tests import VALID_PASSWORD


class TestNewUser():
    """ Creates the 'editor' user
    """

    @classmethod
    def setup_class(cls):
        site = Site.find()
        site.terms_and_conditions['enabled'] = False
        site.save()
        dpl_consent = site.get_consent()
        dpl_consent.enabled = False
        dpl_consent.save()
        cls.site = site

    def test_new_user_form(self, anon_client):
        """ Tests new user form
            Tests self.site.invitation_only
            Tests RESERVED_USERNAMES
            Tests invalid email and passwords
        """
        #logout(client)
        url = "/user/new"
        self.site.invitation_only = True
        self.site.save()
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        self.site.invitation_only = False
        self.site.save()
        response = anon_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert '<a class="nav-link" href="/user/login">' in html
        reserved_username = current_app.config['RESERVED_USERNAMES'][0]
        response = anon_client.post(
                        url,
                        data = {
                            "username": reserved_username,
                            "email": "invalid_email_string",
                            "password": "invalidpassword",
                            "password2": "valid password",
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert '<a class="nav-link" href="/user/login">' in html
        assert html.count('<span class="formError">') == 4

    def test_create_user_account(self, anon_client):
        """Create a standard user."""
        self.site.invitation_only = False
        self.site.save()
        url = "/user/new"
        user = UserFactory()
        response = anon_client.post(
                        url,
                        data = {
                            "username": user.username,
                            "email": user.email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_settings_page -->' in html
        assert '<a class="nav-link" href="/user/logout">' in html
        assert flask_login.current_user.username == user.username
        assert flask_login.current_user.role == 'editor'
        assert flask_login.current_user.validated_email is False
        assert flask_login.current_user.token['email'] == flask_login.current_user.email


class TestUniqueNewUser():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_new_user_form(self, anon_client):
        """Test username and email uniqueness."""
        assert not self.site.invitation_only
        user=User.find() # get any registered user
        url = "/user/new"
        response = anon_client.post(
                        url,
                        data = {
                            "username": user.username,
                            "email": user.email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert html.count('<span class="formError">') == 2
