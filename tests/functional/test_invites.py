"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from copy import copy
import pytest
from flask import current_app
from liberaforms.models.user import User
from liberaforms.models.invite import Invite
from tests.factories import UserFactory
from tests import VALID_PASSWORD, DEFAULT_ROLE
from tests import user_creds
from tests.utils import login, logout


class TestInvites():
    """Create and delete an invitation. Then create again and test new user form"""

    @classmethod
    def setup_class(cls):
        cls.invitee = UserFactory(email=user_creds['invitee']['email'])
        cls.invitation={'id': None, 'token': None}
        cls.invitee_role = 'guest'

    def test_requirements(self):
        assert current_app.config['ENABLE_UPLOADS'] is True


    def test_create_invite_1(self, anon_client, editor_client, admin_client):
        """ Test invite new user
            Test permissions
        """
        url = "/admin/invites/new"
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_invite_page -->' in html

        # TODO: test invitation to existing email
        response = admin_client.post(
                        url,
                        data = {
                            "message": "Hello",
                            "email": user_creds['invitee']['email'],
                            "role": DEFAULT_ROLE
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        if os.environ['SKIP_EMAILS'] == "False":
            assert 'Recipient address rejected:' in html
        else:
            assert '<div class="success flash_message">' in html
        assert '<!-- list_invites_page -->' in html
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        assert new_invite.role == DEFAULT_ROLE
        # make a copy of these values for the next test
        self.invitation['id'] = copy(new_invite.id)
        self.invitation['token'] = new_invite.token['token']
        #print(self.invitation)

    def test_delete_invite_1(self, admin_client):
        url = f"/invite/{self.invitation['id']}/delete"
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        assert '<!-- list_invites_page -->' in html
        assert Invite.find_all().count() == 0

    def test_create_invite_again(self, admin_client):
        url = "/admin/invites/new"
        response = admin_client.post(
                        url,
                        data = {
                            "message": "Hello",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        assert self.invitee_role == new_invite.role
        assert user_creds['invitee']['email'] == new_invite.email
        # make a copy of these values for the next test
        self.invitation['id'] = copy(new_invite.id)
        self.invitation['token'] = new_invite.token['token']

    def test_new_user_form_with_invitation(self, anon_client):
        assert User.find(email=user_creds['invitee']['email']) is None
        url = f"/user/new/{self.invitation['token']}"
        response = anon_client.post(
                        url,
                        data = {
                            "username": "petertheman",
                            "email": user_creds['invitee']['email'],
                            "password": VALID_PASSWORD,
                            "password2":VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        user = User.find(email=user_creds['invitee']['email'])
        assert user.validated_email is True
        assert user.role == self.invitee_role
        assert Invite.find(id=self.invitation['id']) is None
        html = response.data.decode()
        assert '<!-- user_settings_page -->' in html
        assert '<a class="nav-link" href="/user/logout">' in html
        # delete invitee user to continue testing
        user.delete()
        assert User.find(id=user.id) is None

    def test_create_new_admin_invitation(self, anon_client, admin_client):
        """ Test token creation for new admin user
        """
        url = "/admin/invites/new"
        login(admin_client, user_creds['admin'])
        response = admin_client.post(
                        url,
                        data = {
                            "message": "Hello",
                            "email": user_creds['invitee']['email'],
                            "role": "admin",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        if os.environ['SKIP_EMAILS'] == "False":
            assert 'Recipient address rejected' in html
        else:
            assert '<div class="success flash_message">' in html
        assert '<!-- list_invites_page -->' in html
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        assert new_invite.role == "admin"
        # make a copy of these values for the next test
        self.invitation['token'] = new_invite.token['token']

    def test_new_user_form_with_admin_invitation(self, anon_client):
        url = f"/user/new/{self.invitation['token']}"
        response = anon_client.post(
                        url,
                        data = {
                            "username": "petertheman",
                            "email": user_creds['invitee']['email'],
                            "password": VALID_PASSWORD,
                            "password2":VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        user = User.find(email=user_creds['invitee']['email'])
        assert user.validated_email == True
        assert user.role == "admin"
        assert Invite.find_all().count() == 0
        html = response.data.decode()
        assert '<!-- user_settings_page -->' in html
        assert '<a class="nav-link" href="/user/logout">' in html

        #pytest.exit("invite tests completed")
