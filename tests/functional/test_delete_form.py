"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.models.log import FormLog
from tests import user_creds
from tests.factories import FormFactory, AnswerFactory
from tests.assets.form_structure import get_form_structure
from tests.utils import login, random_slug

from pprint import pprint
class TestDeleteForm():
    """Delete a form with answers and attachments."""

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor, anon_client):
        """Create form and answers with attachment."""
        form=FormFactory(author=editor,
                         slug=random_slug(),
                         structure=json.loads(get_form_structure(with_attachment=True)))
        form.enabled = True
        form.save()
        #pprint(form.structure)
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        total_answers=10
        name="Julia"
        answer_cnt = 0
        while answer_cnt < total_answers:
            answer_cnt = answer_cnt +1
            with open(valid_attachment_path, 'rb') as f:
                stream = BytesIO(f.read())
            valid_file = werkzeug.datastructures.FileStorage(
                stream=stream,
                filename=valid_attachment_name,
                content_type=mimetype,
            )
            anon_client.post(
                form.url,
                data = {
                    "text-1620232883208": name,
                    "file-1622045746136": valid_file,
                },
                follow_redirects=True,
            )
        assert form.answers.count() == total_answers
        assert len(os.listdir(form.get_attachment_dir())) == total_answers
        assert form.answers[0].data['text-1620232883208'] == name

    def test_delete_form(self, editor_client):
        """Delete the form and check the answers and attachments are deleted."""
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        initial_answers_count = form.answers.count()
        response = editor_client.get(
                        f"/form/{form.id}/delete",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_form_page -->' in html
        assert f'<span class="highlightedText">{initial_answers_count}' in html
        # test incorrect slug
        response = editor_client.post(
                        f"/form/{form.id}/delete",
                        data = {
                            "slug": f"{form.slug}-wrong"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_form_page -->' in html
        # test correct slug
        response = editor_client.post(
                        f"/form/{form.id}/delete",
                        data = {
                            "slug": form.slug
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- my_forms_page -->' in html
        assert Form.find(id=form.id) is None
        assert Answer.find_all(form_id=form.id).count() == 0
        assert FormUser.find_all(form_id=form.id).count() == 0
        assert AnswerAttachment.find_all(form_id=form.id).count() == 0
        assert os.path.isdir(form.get_attachment_dir()) is False
        assert FormLog.find_all(form_id=form.id).count() == 0
