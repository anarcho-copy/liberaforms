"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import current_app, url_for
from tests.factories import UserFactory
from tests import user_creds
from tests.utils import login, logout
from liberaforms.utils.utils import string_to_bytes


#@pytest.mark.usefixtures("admin_client")
class TestAdministerUsers():
    """ The admin can set some user properties. Test those."""

    @classmethod
    def setup_class(cls):
        cls.guinea_pig_user = UserFactory()
        cls.guinea_pig_user.save()

    def test_set_role(self, anon_client, editor_client, admin_client):
        """ Test permissions
            Test set role 'admin'

        """
        new_role='guest'
        assert self.guinea_pig_user.role != new_role
        url = url_for('admin_bp.set_role', user_id=self.guinea_pig_user.id, role=new_role)
        response = anon_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        initial_role = self.guinea_pig_user.role
        login(admin_client, user_creds['admin'])
        response = admin_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        #is_admin = True if user_creds['tested_user'].role == 'admin' else False
        assert response.json == {"role": new_role}
        assert self.guinea_pig_user.role == new_role
        # reset user.role to continue testing
        #self.guinea_pig_user.role = 'editor'
        #self.guinea_pig_user.save()

    def test_toggle_uploads(self, anon_client, editor_client, admin_client):
        """ Test permissions
            Test toggle
        """

        url = url_for('admin_bp.toggle_uploads_enabled', user_id=self.guinea_pig_user.id)
        response = anon_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        initial_uploads_enabled = self.guinea_pig_user.uploads_enabled
        login(admin_client, user_creds['admin'])
        response = admin_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.guinea_pig_user.role == 'guest'
        assert response.json == {"uploads_enabled": self.guinea_pig_user.uploads_enabled}
        assert initial_uploads_enabled == self.guinea_pig_user.uploads_enabled
        self.guinea_pig_user.role='editor'
        self.guinea_pig_user.save()
        response = admin_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert initial_uploads_enabled != self.guinea_pig_user.uploads_enabled
        # set user.uploads_enabled to False to continue testing
        #user_creds['tested_user'].uploads_enabled = False
        #user_creds['tested_user'].save()

    def test_set_uploads_limit(self, anon_client, editor_client, admin_client):
        url = url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id)
        response = anon_client.get(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_upload_limit_page -->' in html
        over_max_limit = {'size': '10000000', 'unit': 'GB'}
        response = admin_client.post(
                        url,
                        data=over_max_limit,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- inspect_user_page -->' in html
        default_limit = string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT'])
        assert self.guinea_pig_user.uploads_limit == default_limit
        initial_limit = self.guinea_pig_user.uploads_limit
        valid_limit = {'size': '100', 'unit': 'KB'}
        response = admin_client.post(
                        url,
                        data=valid_limit,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- inspect_user_page -->' in html
        assert initial_limit != self.guinea_pig_user.uploads_limit
