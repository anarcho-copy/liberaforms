"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import ast
import pytest
from flask import url_for
import flask_login

from tests import user_creds
from tests.utils import login, logout, is_bool


class TestAdminPreferences():
    """ Tests admin's admin preferences
    """
    def test_toggle_new_user_notification(self, admin_client, anon_client):
        """ Tests admin permission
            Tests POST only
            Tests toggle bool
        """
        url = url_for('admin_bp.toggle_newUser_notification')
        response = anon_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 405
        notification = flask_login.current_user.admin["notifyNewUser"]
        response = admin_client.post(url)
        assert response.status_code == 200
        assert flask_login.current_user.admin["notifyNewUser"] != notification
        assert is_bool(flask_login.current_user.admin["notifyNewUser"])

    def test_toggle_new_form_notification(self, admin_client, anon_client):
        """ Tests admin permission
            Tests POST only
            Tests toggle bool
        """
        url = url_for('admin_bp.toggle_newForm_notification')
        response = anon_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 405
        notification = flask_login.current_user.admin["notifyNewForm"]
        response = admin_client.post(url)
        assert response.status_code == 200
        assert flask_login.current_user.admin["notifyNewForm"] != notification
        assert type(flask_login.current_user.admin["notifyNewForm"]) == type(bool())

    def test_toggle_newuser_uploadsenabled(self, admin_client, anon_client):
        pass
