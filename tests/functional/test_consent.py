"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.consent import Consent
from liberaforms.models.formuser import FormUser
from tests import user_creds
from tests.utils import login, random_slug
from factories import FormFactory


class TestSiteConsent():
    """Test the site's data consent."""

    @classmethod
    def setup_class(cls):
        cls.properties={
            'site_consent': None,
            'form_consent': None
        }
        cls.site = Site.find()

    def test_requirements(self):
        assert Consent.find(site_id=self.site.id) is None

    def test_site_consent_page(self, anon_client, editor_client, admin_client):
        response = anon_client.get(
                        url_for('site_bp.edit_data_consent'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('site_bp.edit_data_consent'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(admin_client, user_creds['admin'])
        response = admin_client.get(
                        url_for('site_bp.edit_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        consent:dict = self.site.get_consent_for_rendering()
        assert consent['html'] in html
        # assert default consent shema values
        assert consent['html'] == self.site.get_default_consent().html
        assert bool(consent['id'] is None and consent['enabled'] is False)

    def test_save_consent_text(self, session, admin_client):
        login(admin_client, user_creds['admin'])
        assert not Consent.find(site_id=1)
        consent = self.site.get_consent()
        assert bool(consent.id is None and consent.html == self.site.get_default_consent().html)
        response = admin_client.post(
                        url_for('site_bp.edit_data_consent'),
                        data = {
                            "markdown": "# Site!",
                            "checkbox_text": ""
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        self.properties['site_consent'] = Consent.find(site_id=1)
        consent:dict = self.site.get_consent_for_rendering()
        assert consent['html'] == "<h1>Site!</h1>"
        assert consent['html'] in html
        assert consent['html'] != self.site.get_default_consent().html
        # Test empty markdown input
        response = admin_client.post(
                        url_for('site_bp.edit_data_consent'),
                        data = {
                            "markdown": "",
                            "checkbox_text": ""
                        },
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        consent:dict = self.site.get_consent_for_rendering()
        assert consent['html'] == self.site.get_default_consent().html
        assert consent['html'] in html
        # this should work but does not. Value in db does not match object value. WTF?
        #assert Consent.find(site_id=self.site.id).html == ""

    def test_toggle_consent_enabled(self, admin_client):
        self.site.get_consent().delete()
        consent=self.site.get_consent()
        assert bool(consent.id is None and consent.enabled is False)
        response = admin_client.post(
                        url_for('site_bp.toggle_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        consent=self.site.get_consent()
        assert consent.enabled == response.json['enabled']
        assert bool(consent.id is not None and consent.enabled is True)
        response = admin_client.get(
                        url_for('site_bp.preview_new_user_form'),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert self.site.get_consent().html in html
        # Toggle and existing consent
        response = admin_client.post(
                        url_for('site_bp.toggle_data_consent'),
                        follow_redirects=False,
                    )
        assert consent.enabled is False
        response = admin_client.get(
                        url_for('site_bp.preview_new_user_form'),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert self.site.get_consent().html not in html

    def test_toggle_consent_shared(self, admin_client):
        self.site.get_consent().delete()
        consent=self.site.get_consent()
        assert bool(consent.id is None and consent.shared is False)
        response = admin_client.post(
                        url_for('site_bp.toggle_share_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        consent=self.site.get_consent()
        assert consent.shared == response.json['shared']
        assert bool(consent.id is not None and consent.shared is True)
        # Toggle and existing consent
        response = admin_client.post(
                        url_for('site_bp.toggle_share_data_consent'),
                        follow_redirects=False,
                    )
        assert consent.shared is False


class TestFormConsent():
    """Test the form's data consent."""

    def setup_class(cls):
        cls.site = Site.find()
        cls.properties={}

    def test_requirements(self, editor, admin_client):
        site_consent=self.site.get_consent()
        site_consent.shared = False
        site_consent.save()
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form
        assert Consent.find(form_id=form.id) is None
        login(admin_client, user_creds['admin'])
        self.site.save_consent({'markdown': "Site consent for form test", 'checkbox_text': ""})
        assert "Site consent for form test" in self.site.get_consent().html

    #def test_new_form_consent(self, editor_client):
    #    """Test new form does not have a corresponding Consent object."""
    #    login(editor_client, user_creds['editor'])
    #    form = self.properties['form']
    #    consent = form.get_consent()
    #    assert consent.id is None
    #    assert consent.markdown == form.get_default_consent().markdown
    #    assert consent.checkbox_text == ""
    #    assert consent.enabled is False
    #    assert consent.required is True

    def test_form_consent_page(self, anon_client, editor_client):
        form = self.properties['form']
        response = anon_client.get(
                        url_for('form_bp.edit_data_consent', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        # get page with site consent.shared = False
        login(editor_client, user_creds['editor'])
        assert self.site.get_consent().shared is False
        response = editor_client.get(
                        url_for('form_bp.edit_data_consent', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        consent:dict = form.get_consent_for_rendering()
        assert consent['id'] is None
        assert consent['html'] != self.site.get_consent_for_rendering()['html']
        assert consent['html'] in response.data.decode()
        # get page again with site consent.shared = True
        self.site.toggle_consent_shared()
        assert self.site.get_consent().shared is True
        consent = form.get_consent()
        assert bool(consent.id is None and consent.html == self.site.get_consent().html)
        response = editor_client.get(
                        url_for('form_bp.edit_data_consent', form_id=form.id),
                        follow_redirects=False,
                    )
        consent:dict = form.get_consent_for_rendering()
        assert bool(consent['id'] is None and consent['html'] == self.site.get_consent().html)
        assert consent['html'] == self.site.get_consent_for_rendering()['html']
        assert consent['html'] in response.data.decode()

    def test_toggle_consent_enabled(self, anon_client, editor_client):
        form = self.properties['form']
        form.enabled = True
        form.save()
        assert form.is_public()
        assert self.site.get_consent().shared is True
        initial_log_count = form.log.count()
        login(editor_client, user_creds['editor'])
        consent=form.get_consent()
        assert bool(consent.id is None and consent.enabled is True) # True because site consent is shared
        response = anon_client.get(
                        url_for('form_bp.view_form', slug=form.slug),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert consent.html in html
        response = editor_client.post(
                        url_for('form_bp.toggle_data_consent', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        consent=form.get_consent()
        assert consent.enabled == response.json['enabled']
        assert bool(consent.id is not None and consent.enabled is False)
        response = anon_client.get(
                        url_for('form_bp.view_form', slug=form.slug),
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert consent.html not in html
        # Set site consent.shared to False and repeat
        self.site.toggle_consent_shared()
        assert self.site.get_consent().shared is False
        consent.delete()
        consent=form.get_consent()
        assert bool(consent.id is None and consent.enabled is False)
        response = editor_client.post(
                        url_for('form_bp.toggle_data_consent', form_id=form.id),
                        follow_redirects=False,
                    )
        consent=form.get_consent()
        assert bool(consent.id is not None and consent.enabled is True)
        # Toggle an existing form consent
        response = editor_client.post(
                        url_for('form_bp.toggle_data_consent', form_id=form.id),
                        follow_redirects=False,
                    )
        assert consent.enabled is False
        assert form.log.count() == initial_log_count + 3


    def test_save_consent_text(self, editor_client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        url_for('form_bp.edit_data_consent', form_id=form.id),
                        data = {
                            "markdown": "# Hello!",
                            "checkbox_text": ""
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        consent = form.get_consent()
        assert consent.html == "<h1>Hello!</h1>"
        assert consent.html in html
        assert consent.html != form.get_default_consent().html
        assert form.log.count() != initial_log_count
        # Test empty markdown input
        response = editor_client.post(
                        url_for('form_bp.edit_data_consent', form_id=form.id),
                        data = {
                            "markdown": "",
                            "checkbox_text": "",
                        },
                        follow_redirects=False,
                    )
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        consent:dict = form.get_consent_for_rendering()
        assert consent['html'] == form.get_default_consent().html
        assert consent['html'] in html
        assert form.log.count() == initial_log_count +2
        # this should work but does not
        #assert Consent.find(form_id=form.id).html == ""
