"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, random_slug


class TestAnswers():

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        while form.answers.count() < 10:
            answer=AnswerFactory(form_id=form.id, author_id=editor.id)
            answer.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_show_answers_table(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('answers_bp.list_answers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- list_answers_page -->' in html

    def test_show_answers_stats(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('answers_bp.answers_stats', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- chart_answers_page -->' in html
        assert '<canvas id="time_chart" height="100"></canvas>' in html

    #def test_answers_enable_edition(self, editor_client):
    #    # data-display function
    #    pass

    def test_download_csv(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        url_for('answers_bp.answers_csv', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.mimetype == 'text/csv'

    def test_toggle_marked_answer(self, editor_client):
        form = self.properties['form']
        answer = form.answers[-1]
        answer_id = vars(answer)['id']
        initial_marked = vars(answer)['marked']
        response = editor_client.post(
                    url_for('data_display_bp.toggle_answer_mark', answer_id=answer_id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['marked'] != initial_marked
        assert vars(answer)['marked'] != initial_marked

    @pytest.mark.skip(reason="No way of currently testing this")
    def test_edit_answer_field(self, editor_client):
        pass

    def test_delete_answer(self, editor_client):
        form = self.properties['form']
        initial_answers_count = form.answers.count()
        assert initial_answers_count > 0
        initial_log_count = form.log.count()
        answer_to_delete = form.answers[-1]
        answer_id = vars(answer_to_delete)['id']
        response = editor_client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer_id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json == True
        assert response.json['deleted'] == True
        assert form.answers.count() == initial_answers_count - 1
        assert form.log.count() == initial_log_count + 1

    def test_delete_all_answers(self, anon_client, editor_client):
        form = self.properties['form']
        initial_answers_count = form.answers.count()
        initial_log_count = form.log.count()
        response = anon_client.get(
                        url_for('answers_bp.delete_answers', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        response = editor_client.get(
                        url_for('answers_bp.delete_answers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_all_answers -->' in html
        assert f'<div class="title_3">{initial_answers_count}</div>' in html
        response = editor_client.post(
                        url_for('answers_bp.delete_answers', form_id=form.id),
                        data = {
                            "totalAnswers": initial_answers_count,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="success flash_message">' in html
        assert '<!-- list_answers_page -->' in html
        assert form.answers.count() == 0
        assert form.log.count() == initial_log_count + 1

    def test_answers_link(self, anon_client, editor_client):
        form = self.properties['form']
        response = anon_client.get(
                        url_for('answers_bp.list_answers', slug=form.slug),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_login_page -->' in html
        response = editor_client.get(
                        url_for('answers_bp.list_answers', slug=form.slug),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- list_answers_page -->' in html
