"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from liberaforms.models.user import User
from liberaforms.models.media import Media
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from tests.factories import FormFactory, UserFactory
from tests.assets.form_structure import get_form_structure
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout, random_slug

class TestDeleteUser():
    """User account delete.

    Creates an answers with attachment and 1 media upload
    Tests the deletion of the user, media, forms and answers and attachments.
    """

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.properties={}
        cls.user = UserFactory(validated_email=True)
        cls.user.save()

    def test_requirements(self, anon_client, editor_client):
        """Create a form an answer with attachment."""
        form=FormFactory(author=self.user,
                         slug=random_slug(),
                         structure=json.loads(get_form_structure(with_attachment=True)))
        form.enabled = True
        form.save()
        form_user = FormUser(
                user_id = self.user.id,
                form_id = form.id,
                is_editor = True,
                notifications = self.user.new_form_notifications()
        )
        form_user.save()
        self.properties['form']=form
        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        # create answer and attachment
        with open(valid_attachment_path, 'rb') as f:
            stream = BytesIO(f.read())
        valid_file = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=valid_attachment_name,
            content_type=mimetype,
        )
        anon_client.post(
            form.url,
            data = {
                "text-1620232883208": "Julia",
                "file-1622045746136": valid_file,
            },
            follow_redirects=True,
        )
        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1
        # upload media
        login(editor_client, {'username': self.user.username, 'password': VALID_PASSWORD})
        url = "/media/save"
        valid_media_name = "valid_media.png"
        valid_media_path = f"./assets/{valid_media_name}"
        with open(valid_media_path, 'rb') as file:
            response = editor_client.post(
                            url,
                            data = {
                                'media_file': file,
                                'alt_text': "valid alternative text",
                            },
                            follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json == True
        # both 1. the uploaded media file and 2. the thumbnail
        assert len(os.listdir(self.user.get_media_dir())) == 2

    def test_delete_account(self, anon_client, editor_client, admin_client):
        """Delete user."""
        url = f"/user/{self.user.id}/delete-account"
        response = anon_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        login(admin_client, user_creds['admin'])
        response = admin_client.post(
                        url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- user_settings_page -->' in html
        login(editor_client, {'username': self.user.username, 'password': VALID_PASSWORD})
        response = editor_client.post(
                        url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_account_page -->' in html
        assert f"<td>{self.user.authored_forms_total()}</td>" in html
        assert f"<td>{self.user.get_answers().count()}</td>" in html
        wrong_username=user_creds['admin']['username']
        response = editor_client.post(
                        url,
                        data = {
                            'delete_username': wrong_username,
                            'delete_password': VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_account_page -->' in html
        wrong_password=VALID_PASSWORD+'_'
        response = editor_client.post(
                        url,
                        data = {
                            'delete_username': self.user.username,
                            'delete_password': wrong_password,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_account_page -->' in html
        response = editor_client.post(
                        url,
                        data = {
                            'delete_username': self.user.username,
                            'delete_password': VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- site_index_page -->' in html
        assert User.find(id=self.user.id) is None
        assert FormUser.find_all(user_id=self.user.id).count() == 0
        assert Form.find_all(author_id=self.user.id).count() == 0
        assert Answer.find_all(author_id=self.user.id).count() == 0
        assert Media.find(user_id=self.user.id) is None
