"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
import flask_login
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests import user_creds
from tests.utils import login, random_slug
from factories import FormFactory

class TestFormSettings():
    """Test the settings an editor can set on a form."""

    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor):
        form=FormFactory(author=editor, slug=random_slug())
        form.save()
        form_user = FormUser(
                user_id = editor.id,
                form_id = form.id,
                is_editor = True,
                notifications = editor.new_form_notifications()
        )
        form_user.save()
        self.properties['editor']=editor
        self.properties['form']=form

    def test_toggle_public(self, editor_client):
        """Tests Form.enabled bool.
           Tests for a new FormLog entry
        """
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        initial_enabled = form.enabled
        initial_log_count = form.log.count()
        response = editor_client.post(
                        f"/form/toggle-enabled/{form.id}",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert form.enabled != initial_enabled
        assert response.json['enabled'] == form.enabled
        assert form.log.count() == initial_log_count + 1

    def test_toggle_new_answer_notification(self, editor_client):
        form = self.properties['form']
        form_user=FormUser.find(form_id=form.id,
                                user_id=flask_login.current_user.id,
                                is_editor=True)
        initial_preference = form_user.notifications['newAnswer']
        #login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form.id}/toggle-notification",
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert initial_preference != response.json['notification']
        saved_preference = form_user.notifications['newAnswer']
        assert saved_preference != initial_preference
        assert type(saved_preference) == type(bool())

    def test_save_expired_text(self, editor_client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        #login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form.id}/save-expired-text",
                        data = {
                            "markdown": "# Hello",
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['html'] == "<h1>Hello</h1>"
        assert form.get_expired_text_html() == response.json['html']
        assert form.log.count() != initial_log_count

    def test_save_after_submit_text(self, editor_client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        login(editor_client, user_creds['editor'])
        response = editor_client.post(
                        f"/form/{form.id}/save-after-submit-text",
                        data = {
                            "markdown": "# Hello",
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['html'] == "<h1>Hello</h1>"
        assert form.get_after_submit_text_html() == response.json['html']
        assert form.log.count() != initial_log_count

    def test_view_log(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        f"/forms/log/list/{form.id}",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- log_list_page -->" in html

    def test_duplicate_form(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        f"/form/{form.id}/duplicate",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<div class="info flash_message">' in html
        assert form.introduction_text['markdown'] in html
        assert '<input id="slug" value=""' in html

    def test_embed_form_html_code(self, editor_client):
        form = self.properties['form']
        login(editor_client, user_creds['editor'])
        response = editor_client.get(
                        f"/form/{form.id}",
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert form.get_embed_url() in html
