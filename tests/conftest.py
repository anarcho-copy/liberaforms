"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from tests import user_creds, VALID_PASSWORD
import os, ast, time
import pytest
import flask_migrate# import Migrate, upgrade, stamp
from liberaforms import create_app
from liberaforms import db as _db
from liberaforms.models.site import Site
from liberaforms.models.user import User


@pytest.fixture(scope='session')
def app():
    """Return app."""
    flask_app = create_app()
    yield flask_app

@pytest.fixture(scope='session')
def db(app):
    """Upgrade database schema with alembic.

    Yields db
    Drops tables
    """
    migrate = flask_migrate.Migrate(app, _db, directory='../migrations')
    with app.app_context():
        _db.drop_all()
        flask_migrate.stamp(revision='base')
        flask_migrate.upgrade()
        yield _db
        _db.session.close()
        #_db.drop_all()


@pytest.fixture(scope='session')
def session(db):
    connection = db.engine.connect()
    #transaction = connection.begin()

    options = dict(bind=connection)
    session = db.create_scoped_session(options=options)
    db.session = session
    yield session



@pytest.fixture(autouse=True)
def run_around_tests():
    # Code that will run before your test
    # <- here
    yield
    # Code that will run after your test
    #time.sleep(0.2)

#@pytest.fixture(scope="module")
##@pytest.fixture(autouse=True)
#def site(session):
#    yield Site.find()

@pytest.fixture(scope='session')
def users():
    return user_creds

from factories import UserFactory



@pytest.fixture(scope='function')
def root_user(db):
    user = User.find(username=user_creds['root_user']['username'])
    if user:
        return user
    user = UserFactory(username=user_creds['root_user']['username'],
                       password=VALID_PASSWORD,
                       email=user_creds['root_user']['email'],
                       validated_email=True,
                       role='admin')
    user.save()
    return user

@pytest.fixture(autouse=True)
def admin(db):
    user = User.find(username=user_creds['admin']['username'])
    if user:
        return user
    user = UserFactory(username=user_creds['admin']['username'],
                       password=VALID_PASSWORD,
                       email=user_creds['admin']['email'],
                       validated_email=True,
                       role='admin')
    user.save()
    return user

@pytest.fixture(autouse=True)
def editor(db):
    user = User.find(username=user_creds['editor']['username'])
    if user:
        return user
    user = UserFactory(username=user_creds['editor']['username'],
                       password=VALID_PASSWORD,
                       email=user_creds['editor']['email'],
                       validated_email=True,
                       role='editor')
    user.uploads_enabled = True
    user.save()
    return user

import flask_login

@pytest.fixture(scope='function')
def authenticated_admin(app, admin):
    with app.test_request_context():
        # Here we're not overloading the login manager, we're just directly logging in a user
        # with whatever parameters we want. The user should only be logged in for the test,
        # so you're not polluting the other tests.
        yield flask_login.login_user(admin)

@pytest.fixture(scope='function')
def authenticated_editor(app, editor):
    with app.test_request_context():
        # Here we're not overloading the login manager, we're just directly logging in a user
        # with whatever parameters we want. The user should only be logged in for the test,
        # so you're not polluting the other tests.
        yield flask_login.login_user(editor)
