"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json

def get_form_structure(with_email=False, with_attachment=False):
    if with_attachment:
        return json.dumps(structure_with_attachment)
    if with_email:
        return json.dumps(structure_with_email)
    return json.dumps(structure)

structure = [
   {
     "type": "text",
     "required": False,
     "label": "Name",
     "className": "form-control",
     "name": "text-1620232883208",
     "subtype": "text"
   },
   {
     "type": "date",
     "required": False,
     "label": "Date",
     "className": "form-control",
     "name": "date-1620224710459"
   },
   {
     "type": "number",
     "required": False,
     "label": "Number",
     "className": "form-control",
     "name": "number-1620224716308"
   }
 ]

structure_with_email = [
   {
     "type": "text",
     "required": True,
     "label": "Name",
     "className": "form-control",
     "name": "text-1620232883208",
     "subtype": "text"
   },
   {
     "type": "date",
     "required": False,
     "label": "Date",
     "className": "form-control",
     "name": "date-1620224710459"
   },
   {
     "type": "number",
     "required": False,
     "label": "Number",
     "className": "form-control",
     "name": "number-1620224716308"
   },
   {
     "type": "text",
     "subtype": "email",
     "required": False,
     "label": "Email",
     "className": "form-control",
     "name": "text-1620232903350"
   }
 ]

structure_with_attachment = [
   {
     "type": "text",
     "required": False,
     "label": "Name",
     "className": "form-control",
     "name": "text-1620232883208",
     "subtype": "text"
   },
   {
     "type": "date",
     "required": False,
     "label": "Date",
     "className": "form-control",
     "name": "date-1620224710459"
   },
   {
     "type": "number",
     "required": False,
     "label": "Number",
     "className": "form-control",
     "name": "number-1620224716308"
   },
   {
     "type": "text",
     "subtype": "email",
     "required": False,
     "label": "Email",
     "className": "form-control",
     "name": "text-1620232903350"
   },
   {
     "className": "form-control",
     "label": "upload a file",
     "multiple": False,
     "name": "file-1622045746136",
     "required": True,
     "subtype": "file",
     "type": "file"
   }
 ]
