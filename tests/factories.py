"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
import factory
from factory.fuzzy import FuzzyText
from tests import VALID_PASSWORD
from tests.assets.form_structure import get_form_structure
#from liberaforms import db
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.answer import Answer
from liberaforms.utils import sanitizers

# used to create consent_texts
from liberaforms import create_app
from flask import has_app_context, g
from liberaforms.models.site import Site

#factory.random.reseed_random('my awesome project')

# random # IDEA:
# foo = factory.LazyAttribute(lambda a: MyModel.objects.order_by('?').first())

def _create_introduction_text(markdown:str):
    return {'markdown': markdown, 'html': sanitizers.markdown2HTML(markdown)}

def _get_consent_text():
    if has_app_context():
        return Form.new_data_consent()
    with create_app().app_context():
        g.site=Site.find()
        return Form.new_data_consent()

#def fuzzy_text(length=24):
#    return str(uuid.uuid4())
#    return FuzzyText(length=length).fuzz().lower()
#    with create_app().app_context():
#        slug = FuzzyText(length=length).fuzz().lower()
#        while Form.find(slug=slug):
#            slug = FuzzyText(length=length).fuzz().lower()
#        return slug

class UserFactory(factory.Factory):
    class Meta:
        model = User

    username=factory.Faker('user_name')
    email=factory.Faker('email')
    password=VALID_PASSWORD
    validated_email=False


#class FormFactory(factory.alchemy.SQLAlchemyModelFactory):
class FormFactory(factory.Factory):
    class Meta:
        model = Form
        #sqlalchemy_session = db.session

    slug=None
    structure=json.loads(get_form_structure())
    fieldIndex=Form.create_field_index(structure)
    introduction_text=_create_introduction_text(Form.default_introduction_text())
    after_submit_text={'html':"", 'markdown':""}
    expired_text={'html':"", 'markdown':""}
    author=None

class AnswerFactory(factory.Factory):
    class Meta:
        model = Answer

    form_id=None
    author_id=None
    data={}
